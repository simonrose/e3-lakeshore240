program ls240Curve

// This finite state machine dose not use Asyn. The Stream uses Asyan


option +r;

%%#include <stdio.h>
%%#include <stdlib.h>
%%#include <string.h>
%%#include <termios.h>
%%#include <unistd.h>
%%#include <stdbool.h>
%%#include <cantProceed.h>
%%#include <ctype.h>
%%#include <sys/stat.h>
%%#include <fcntl.h>
%%#include <sys/signal.h>
%%#include <sys/types.h>
//#include <sys/stat.h>
//#include <linux/kernel.h>
%%#include <termios.h>
%%#include <stdarg.h>
%%#include <time.h>      // for nanosleep()
%%#include <stdbool.h>
%%#include <dirent.h>
%%#include <ellLib.h> 
%%#include <epicsString.h>
%%#include <epicsEvent.h>
%%#include <asynDriver.h>
%%#include <asynOctet.h>
%%#include <asynOctetSyncIO.h>
%%#include "ls240Curve.h"

// pvs
string input;  assign input     to "{P}:calibrationInput";
string output; assign output    to "{P}:calibrationOutput";
int connected; assign connected to "{P}:connected";
int scanEnableDisable; assign scanEnableDisable to "{user}:{P}:scanEnableDisable";

// Variables
char *listenerPortName;
char *IOPortName;
char *asynPortName;
int readStatus;
int writeStatus;
char *pasynUser;    /* This is really asynUser* */
char *registrarPvt; /* This is really void* */
char *eventId;      /* Thus is really epicsEventId */
unsigned int idex;  /* PV index, initialize at init phase, can not use VAR_ID */
unsigned int odex;
unsigned int scandex;
int com_fd;
char rcv_buff[256];
char *serial_port = "/dev/tty.SLAB_USBtoUART";
const char *curve_dir = "/Users/juntongliu/epics/epics-apps/IOC_LS240Curve/curve/"; 

// Function prototype
%%static int init_serial_ports(SS_ID ssId, struct UserVar *pVar);
%%static int wait_10ms(void);
%%static int wait_2seconds(void);
%%static int ls240_queryDeviceID(SS_ID ssId, struct UserVar *pVar);
%%static int ls240_getReply(SS_ID ssId, struct UserVar *pVar);
%% void freeNodes(void *pnode);
// %%static int pickOutToken(char **token, char *cp, const char *delimiter);
// %%static int parse_curve(char *fileName, SS_ID ssId, struct UserVar *pVar);
// %%static int ls240_queryIntype(int inNum, char *inputType, SS_ID ssId, struct UserVar *pVar);
// %%static int ls240_queryCRVPT(int inNum, int index, SS_ID ssId, struct UserVar *pVar);
// %%static int ls240_queryCRVHDR(char *crvHeader, int input, SS_ID ssId, struct UserVar *pVar);
// %%static int list_curveFiles(SS_ID ssId, struct UserVar *pVar);
// %%static int upload_curve(int inNum, SS_ID ssId, struct UserVar *pVar);
// %%static int do_uploading(SS_ID ssId, struct UserVar *pVar) ;

/*
%%static int print_parse_result(void);
%%static int ls240_dumpSaveCurve(char *fileName, int inNum);
*/


ss LS240SS
{
    state init 
    {
        when() 
        {
            %%init_serial_ports(ssId, pVar);
            connected = 0;
            idex = pvIndex(input);
            odex = pvIndex(output);
            scandex = pvIndex(scanEnableDisable);
        } state waitCommands
    }

    state waitCommands 
    {
        when() 
        {
            connected = 1;
            pvPut(connected);
            //pvPut(connected);
        } state processCommands
    }

    state processCommands 
    {

        when(connected) 
        {
            scanEnableDisable = 1;
            pvPut(scanEnableDisable);
            %%wait_2seconds();
            %%pVar->writeStatus = ls240_queryDeviceID(ssId, pVar); 
            pvPut(output);
            if (writeStatus == 0) 
            {
                pvPut(input);
            }
            scanEnableDisable = 0;
            pvPut(scanEnableDisable);
            //%%wait_10ms();
           //} state processCommands
        } state done

        when(!connected) 
        {
        } state waitCommands    // waitConnect, this waitConnect state does not exist, it will cause strange linker error!!!!
 
    }

    state done
    {
        when()
        {

        } state done
    }
}


%{

/**
 * @brief  Free the heap memory
 * @note   
 * @param  *pnode: 
 * @retval None
 */
void freeNodes(void *pnode)
{
    free(pnode);
}

/**
 * @brief  Pick out tokens from a string
 * @note   
 * @param  **token: 
 * @param  *cp: 
 * @param  *delimiter: 
 * @retval 
 */
static int pickOutToken(char **token, char *cp, const char *delimiter)
{
    int i = 0;

    while (*cp == ' ' || *cp == '\t')
        cp++;
    while ((token[i] = strsep(&cp, delimiter)))
    {
        while (*cp == ' ' || *cp == '\t')
            cp++;
        if (*cp == '\0' || *cp == '\n' || *cp == '\r')
            break;
        i++;
    };
    return i + 1;
}

/**
 * @brief  Wait 10 millioseconds
 * @note   
 * @retval 
 */
static int wait_10ms(void)
{
    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = 1e+7;                 // 10ms  milliom second, 1000,000,000 nanoseconds = 1 second
    return nanosleep(&tim, &tim2);
}

/**
 * @brief  Wait 2 seconds
 * @note   
 * @retval 
 */
static int wait_2seconds(void)
{
    struct timespec tim, tim2;
    tim.tv_sec = 1.5;
    tim.tv_nsec = 0;                 // 10ms  milliom second, 1000,000,000 nanoseconds = 1 second
    return nanosleep(&tim, &tim2);
}

/**
 * @brief  Initialize the serial port
 * @note   
 * @param  ssId: 
 * @param  *pVar: 
 * @retval 
 */
static int init_serial_ports(SS_ID ssId, struct UserVar *pVar)
{

   struct termios newOpt, oldOpt; // options;
   // Open the virtual port
   pVar->com_fd = open(pVar->serial_port, O_RDWR | O_NOCTTY | O_NONBLOCK);
   //com_fd = open(serial_port, O_RDWR | O_NOCTTY);
   if (com_fd < 0)
   {
      printf("################################\n");
      printf("Error opening USB virtual port!!!\n");
      printf("################################\n");
      return -1;
   };

   bzero(&newOpt, sizeof(struct termios));
   /* set terminal attributes.  */
   //  fcntl(com_fd[i], F_SETFL, 0);
   tcgetattr(com_fd, &oldOpt);
   cfsetispeed(&newOpt, 115200);
   cfsetospeed(&newOpt, 115200);

 
   newOpt.c_cflag |= (CS8 | CLOCAL | CREAD);
   newOpt.c_iflag &= ~(IXON | IXOFF | IXANY);      // disable software flow control
   newOpt.c_oflag = 0;                             // oflag=0, use raw output
   newOpt.c_cc[VTIME] = 2;                         // set terminal i/o time out, 2 ok, 5 do not work
   newOpt.c_cc[VMIN] = 0;                          // blocking read util minimum 5 char received

// Set close on exec flag
#if defined(FD_CLOEXEC)
   if (fcntl(com_fd, F_SETFD, FD_CLOEXEC) < 0)
   {
      dprintf( "Can't set close-on-exec flag!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
#endif 
      
   // Clean the line and active new setting
   tcflush(pVar->com_fd, TCIFLUSH);

   int tcret = tcsetattr(pVar->com_fd, TCSANOW, &newOpt); 
   if(tcret < 0)
   {
      printf("tcsetattr() failed!\n");
	  close(pVar->com_fd);
      pVar->com_fd = -1;
      return -1;
   }
   //dprintf("com_fd = %d\n; ", com_fd);

   tcflush(pVar->com_fd, TCIOFLUSH);
   //tty->readTimeout = -1e-99;
   //tty->writeTimeout = -1e-99;
   if (fcntl(pVar->com_fd, F_SETFL, 0) < 0)
   {
      dprintf("Can't set F_SETFL file flags!\n");
      close(pVar->com_fd);
      pVar->com_fd = -1;
      return -1;
   }
   //dprintf("\n Port fd=%d opened and options set\n", com_fd);
   
   return 0; 
}

/**
 * @brief  find all curve files in the curve/ directory 
 * @note   
 * @retval 
 */
static int list_curveFile(SS_ID ssId, struct UserVar *pVar)
{
    DIR *dp = NULL;
    struct dirent *dptr = NULL;
    //char buff[256];      // buffer for storing the directory path
    //memset(buff, 0, sizeof(buff));

    //strncpy(buff, argv[1], strlen(curve_dir));
    // Open the directory
    if(!(dp = opendir(curve_dir)))
    {
        printf("Error open directory: %s!\n", curve_dir);
        //exit(1);
        return -1;
    }

    // Read the contents of the dir
    while((dptr = readdir(dp)))
    {
        // skip the . and ..
        if(strlen(dptr->d_name) == 1 && dptr->d_name[0] == '.')
            continue;
        else if(strlen(dptr->d_name) == 2 && dptr->d_name[0] == '.' && dptr->d_name[1] == '.')
            continue;
        else if(strstr(dptr->d_name, ".340"))
        {

        }
    }

    return 0;
}

/**
 * @brief  Get the device reply
 * @note   
 * @param  ssId: 
 * @param  *pVar: 
 * @retval 
 */
static int ls240_getReply(SS_ID ssId, struct UserVar *pVar)
{
   ssize_t rw_ret;
   char buff[8];
   char rcv_buff[256];
   int tries = 0, ret;
   ret = wait_10ms();
   DBGPRINT("nanosleep() returned with: %d\n", ret);
   while (1)
   {
      //pos++;
      tries++;
      rw_ret = read(pVar->com_fd, rcv_buff, 256 - 1);             /* read the reply */
      if (strstr(rcv_buff, "\r\n") || strchr(rcv_buff, '\n'))
         break;
      if (rw_ret < 0)
      {
         perror("Read error from 240!");
         close(pVar->com_fd);
         pVar->com_fd = -1;
         return -1;
      }
   }

   // For a kind of flow control
   snprintf(buff, 8, "%c", '\n');
    rw_ret = write(pVar->com_fd, buff, strlen(buff));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(pVar->com_fd);
        return -1;
    }
    printf("Get the device reply after %d read() tries\n", tries);
    // pass it to pv
    strncpy(pVar->input, rcv_buff, 40);
    seq_pvPut(ssId, pVar->idex, SYNC);

   return 0;
}



/**
 * @brief  Query the device ID (for test scan enable/disable only) 
 * @note   
 * @param  ssId: 
 * @param  *pVar: 
 * @retval 
 */
static int ls240_queryDeviceID(SS_ID ssId, struct UserVar *pVar)
{
   ssize_t rw_ret;
   char strCMD[8];
   snprintf(strCMD, 8, "%s", "*IDN?\n");
   tcflush(pVar->com_fd, TCIOFLUSH);
   rw_ret = write(pVar->com_fd, strCMD, 6);
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(pVar->com_fd);
      pVar->com_fd = -1;
      return -1;
   }
   
    strncpy(pVar->output, strCMD, strlen(strCMD));
    seq_pvPut(ssId, pVar->odex, SYNC);

   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
   dprintf("Device ID query, cmd=%s\n", "*IDN?");
   // get device reply
   if(ls240_getReply(ssId, pVar) < 0)
   {
      printf("Error reading device reply\n");
   }
   else { 
      printf("\nState Machine gets Device ID is: %s\n", pVar->input);
      
   }
   //memset(pVar->rcv_buff, 0, 256);

   return 0;
}




}%
