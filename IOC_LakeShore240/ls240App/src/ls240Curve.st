/*
 *  Juntong Liu
 *  @ European Spallation Source (ESS)
 *  Email: juntong.liu@eu.se
 *                                     2020.10.03
 *  ----------------------------------------------
 * 
 * The IOC for LakeShore240 is consist of two planes: 1.) monitor plane, 2.) control plane.  
 * This finite state machine program is the main part of the control plane of the IOC for LakeShore-240. The main tasks
 *  of this finite state machine is to take commands from the operator and carry out corresponding actions like channel curve 
 *  uploading, disconnect profibus connection, channel parameter setting, ....etc.
 * 
 *  The program is still under development.....
 */ 

program ls240Curve
//program ls240Curve("P=ls240Curve:=, PORT=L0")

//program ls240Curve("P=testIPServer:=, PORT=L0")

option +r;

%%#include <stdio.h>
%%#include <stdlib.h>
%%#include <string.h>
%%#include <termios.h>
%%#include <unistd.h>
%%#include <stdbool.h>
%%#include <cantProceed.h>
%%#include <ctype.h>
%%#include <sys/stat.h>
%%#include <fcntl.h>
%%#include <sys/signal.h>
%%#include <sys/types.h>
//#include <sys/stat.h>
//#include <linux/kernel.h>
%%#include <termios.h>
%%#include <stdarg.h>
%%#include <time.h>      // for nanosleep()
%%#include <stdbool.h>
%%#include <dirent.h>
%%#include <ellLib.h> 
%%#include <epicsString.h>
%%#include <epicsEvent.h>
%%#include <asynDriver.h>
%%#include <asynOctet.h>
%%#include <asynOctetSyncIO.h>
%%#include "ls240Curve.h"


// Connected variables
string input;  assign input     to "{P}:calibrationInput";
string output; assign output    to "{P}:calibrationOutput";
int connected; assign connected to "{P}:connected";
////string addrOnMainDisplay; assign to "{user}:{P}:ADDR";      
int scanEnableDisable; assign scanEnableDisable to "{user}:{P}:scanEnableDisable";
string ssStatusMessage; assign ssStatusMessage to "{user}:{P}:ssStatusMessage";
// string curveFileName; assign curveFileName to "{user}:{P}:curveFileName";
// unsigned int curveUploadCMD; assign curveUploadCMD to "{user}:{P}:curveUploadCMD";
unsigned short profibusAddress; assign profibusAddress to "{user}:{P}:profibusAddress";
string testDeviceID; assign testDeviceID to "{user}:{P}:testDeviceID";
unsigned short commandGetDevId; assign commandGetDevId to "{user}:{P}:commandGetDevId"; 
unsigned short commandDisconProfibus; assign commandDisconProfibus to "{user}:{P}:commandDisconProfibus";
unsigned short commandSetProfibusAddr; assign commandSetProfibusAddr to "{user}:{P}:commandSetProfibusAddr";
unsigned short commandUploadCurve; assign commandUploadCurve to "{user}:{P}:commandUploadCurve";
unsigned short selectedChannel; assign selectedChannel to "{user}:{P}:selectedChannel"; 
unsigned short curveFileSequenceNum; assign curveFileSequenceNum to "{user}:{P}:curveFileSequenceNum";

string channel1CurveSerial; assign channel1CurveSerial to "{user}:{P}:channel1CurveSerial";
string channel2CurveSerial; assign channel2CurveSerial to "{user}:{P}:channel2CurveSerial";
string channel3CurveSerial; assign channel3CurveSerial to "{user}:{P}:channel3CurveSerial";
string channel4CurveSerial; assign channel4CurveSerial to "{user}:{P}:channel4CurveSerial";
string channel5CurveSerial; assign channel5CurveSerial to "{user}:{P}:channel5CurveSerial";
string channel6CurveSerial; assign channel6CurveSerial to "{user}:{P}:channel6CurveSerial";
string channel7CurveSerial; assign channel7CurveSerial to "{user}:{P}:channel7CurveSerial";
string channel8CurveSerial; assign channel8CurveSerial to "{user}:{P}:channel8CurveSerial";

/* channelXIntypeFlag:  bit0 => enable/disable; bit1 => auto range; bit3 => current reversal
 * So, this is actually for the 3 on/off buttons parts of the input type */
unsigned int channel1IntypeBits; assign channel1IntypeBits to "{user}:{P}:channel1IntypeFlag";
unsigned int channel2IntypeBits; assign channel2IntypeBits to "{user}:{P}:channel2IntypeFlag";
unsigned int channel3IntypeBits; assign channel3IntypeBits to "{user}:{P}:channel3IntypeFlag";
unsigned int channel4IntypeBits; assign channel4IntypeBits to "{user}:{P}:channel4IntypeFlag";
unsigned int channel5IntypeBits; assign channel5IntypeBits to "{user}:{P}:channel5IntypeFlag";
unsigned int channel6IntypeBits; assign channel6IntypeBits to "{user}:{P}:channel6IntypeFlag";
unsigned int channel7IntypeBits; assign channel7IntypeBits to "{user}:{P}:channel7IntypeFlag";
unsigned int channel8IntypeBits; assign channel8IntypeBits to "{user}:{P}:channel8IntypeFlag";

// Input type in main window
string channel1InType; assign channel1InType to "{user}:{P}:INTYPE";
string channel2InType; assign channel2InType to "{user}:{P}:INTYPE2";
string channel3InType; assign channel3InType to "{user}:{P}:INTYPE3";
string channel4InType; assign channel4InType to "{user}:{P}:INTYPE4";
string channel5InType; assign channel5InType to "{user}:{P}:INTYPE5";
string channel6InType; assign channel6InType to "{user}:{P}:INTYPE6";
string channel7InType; assign channel7InType to "{user}:{P}:INTYPE7";
string channel8InType; assign channel8InType to "{user}:{P}:INTYPE8";

string curveFileToUpload; assign curveFileToUpload to "{user}:{P}:curveFileToUpload";
////struct curveFiles curveFiles; assign curveFiles to "{user}:{P}:curveFiles"; 
string curveFilesArray[20]; assign curveFilesArray to "{user}:{P}:curveFilesArray";
unsigned int commandListCurves; assign commandListCurves to "{user}:{P}:commandListCurves";

// For monitor variables
unsigned int originalProfibusAddr; assign originalProfibusAddr to "{user}:{P}:ADDR";

monitor commandGetDevId;
monitor commandDisconProfibus;
monitor commandSetProfibusAddr;
monitor commandListCurves;
monitor commandUploadCurve;
// we monitor the change of the variable
monitor channel1IntypeBits;
monitor channel2IntypeBits;
monitor channel3IntypeBits;
monitor channel4IntypeBits;
monitor channel5IntypeBits;
monitor channel6IntypeBits;
monitor channel7IntypeBits;
monitor channel8IntypeBits;


// Variables
int readStatus;
int writeStatus;

unsigned int inputIndex;  /* PV index, initialize at init phase, can not use VAR_ID */
unsigned int outputIndex;
////unsigned int scanOnOffIndex;
unsigned int ssStatusMsgIndex;
unsigned int testDeviceIDIndex;
unsigned int profibusAddressIndex;
unsigned int selectedChannelIndex;
unsigned int curveFilesArrayIndex;
unsigned int curveFileSequenceNumIndex;
unsigned int originalProfibusAddrIndex;

unsigned int channel1CurveSerialIndex; 
unsigned int channel2CurveSerialIndex; 
unsigned int channel3CurveSerialIndex; 
unsigned int channel4CurveSerialIndex; 
unsigned int channel5CurveSerialIndex; 
unsigned int channel6CurveSerialIndex; 
unsigned int channel7CurveSerialIndex; 
unsigned int channel8CurveSerialIndex; 

unsigned int channel1IntypeBitsIndex;
unsigned int channel2IntypeBitsIndex;
unsigned int channel3IntypeBitsIndex;
unsigned int channel4IntypeBitsIndex;
unsigned int channel5IntypeBitsIndex;
unsigned int channel6IntypeBitsIndex;
unsigned int channel7IntypeBitsIndex;
unsigned int channel8IntypeBitsIndex;

// for OPI
unsigned int channel1InTypeIndex;
unsigned int channel2InTypeIndex;
unsigned int channel3InTypeIndex;
unsigned int channel4InTypeIndex;
unsigned int channel5InTypeIndex;
unsigned int channel6InTypeIndex;
unsigned int channel7InTypeIndex;
unsigned int channel8InTypeIndex;


unsigned int curveFileToUploadIndex;

// Channel number on which an on/off button has been clicked
unsigned short onoffCMD;

////unsigned int addrOnMainDisplayIndex;

////unsigned int commandGetDevIdIndex;
int com_fd;

// Strange, the following line can not be here!!!!, before the function use it is OK. It can not be a const here
//const char *curve_dir = "/Users/juntongliu/epics/epics-apps/IOC_LS240Curve/curve/";

// Function prototypes
%%void freeNodes(void *pnode);
%%extern int wait_10ms(void);
%%extern int wait_1second(void);
%%extern int init_serial_ports(SS_ID ssId, struct UserVar *pVar);
%%extern int ls240_getReply(SS_ID ssId, struct UserVar *pVar);
%%extern int pickOutToken(char **token, char *cp, const char *delimiter);
%%extern int print_parse_result(void);
%%extern int ls240_queryIntype(int inNum, char *inputType, SS_ID ssId, struct UserVar *pVar);
%%extern int upload_curve(SS_ID ssId, struct UserVar *pVar);
%%static int ls240_queryDeviceID(SS_ID ssId, struct UserVar *pVar);
%%static int list_curveFiles(int inNum, SS_ID ssId, struct UserVar *pVar);
%%static int ls240_queryADDR(SS_ID ssId, struct UserVar *pVar);
%%static int ls240_setAddr(int inNum, SS_ID ssId, struct UserVar *pVar);
%%static int ls240_queryCRVHDR(char *crvHeader, int input, SS_ID ssId, struct UserVar *pVar);
//%%static int process_3onOff(int inNum, SS_ID ssId, struct UserVar *pVar);
%%static int do_3onoff_process(int inNum, SS_ID ssId, struct UserVar *pVar);
%%static int update_onoff_tracker(int inNum, SS_ID ssId, struct UserVar *pVar);
%%static int compare_onoff_3fields(int inNum, SS_ID ssId, struct UserVar *pVar, char **tokkens);


/*
%%static int print_parse_result(void);
%%static int ls240_dumpSaveCurve(char *fileName, int inNum); */

ss LS240SS
{
    state init 
    {
        when() 
        {
            %%init_serial_ports(ssId, pVar);
            connected = 0;
            inputIndex = pvIndex(input);
            outputIndex = pvIndex(output);
            ////scanOnOffIndex = pvIndex(scanEnableDisable);
            ssStatusMsgIndex = pvIndex(ssStatusMessage);
            testDeviceIDIndex = pvIndex(testDeviceID);
            profibusAddressIndex = pvIndex(profibusAddress);               // from operator for set new
            originalProfibusAddrIndex = pvIndex(originalProfibusAddr);     // the one before change/disconnect
            selectedChannelIndex = pvIndex(selectedChannel);
            curveFilesArrayIndex = pvIndex(curveFilesArray);
            curveFileSequenceNumIndex = pvIndex(curveFileSequenceNum);

            channel1CurveSerialIndex = pvIndex(channel1CurveSerial);
            channel2CurveSerialIndex = pvIndex(channel2CurveSerial);
            channel3CurveSerialIndex = pvIndex(channel3CurveSerial);
            channel4CurveSerialIndex = pvIndex(channel4CurveSerial);
            channel5CurveSerialIndex = pvIndex(channel5CurveSerial);
            channel6CurveSerialIndex = pvIndex(channel6CurveSerial);
            channel7CurveSerialIndex = pvIndex(channel7CurveSerial);
            channel8CurveSerialIndex = pvIndex(channel8CurveSerial);
            
            curveFileToUploadIndex = pvIndex(curveFileToUpload);

            channel1IntypeBitsIndex = pvIndex(channel1IntypeBits);
            channel2IntypeBitsIndex = pvIndex(channel2IntypeBits);
            channel3IntypeBitsIndex = pvIndex(channel3IntypeBits);
            channel4IntypeBitsIndex = pvIndex(channel4IntypeBits);
            channel5IntypeBitsIndex = pvIndex(channel5IntypeBits);
            channel6IntypeBitsIndex = pvIndex(channel6IntypeBits);
            channel7IntypeBitsIndex = pvIndex(channel7IntypeBits);
            channel8IntypeBitsIndex = pvIndex(channel8IntypeBits);
            
            channel1InTypeIndex = pvIndex(channel1InType);
            channel2InTypeIndex = pvIndex(channel2InType);
            channel3InTypeIndex = pvIndex(channel3InType);
            channel4InTypeIndex = pvIndex(channel4InType);
            channel5InTypeIndex = pvIndex(channel5InType);
            channel6InTypeIndex = pvIndex(channel6InType);
            channel7InTypeIndex = pvIndex(channel7InType);
            channel8InTypeIndex = pvIndex(channel8InType);


            ////addrOnMainDisplayIndex = pvIndex(addrOnMainDisplay);
            ////commandGetDevIdIndex = pvIndex(commandGetDevId);
            commandListCurves = 0;
            commandGetDevId = 0;
            commandDisconProfibus = 0;
            commandSetProfibusAddr = 0;
            commandUploadCurve = 0;
            curveFileSequenceNum = 0;
// ----------
            // scanEnableDisable = 1;
            // pvPut(scanEnableDisable);
            // %%wait_1second();
            // pvGet(commandListCurves);
            // commandGetDevId = 0;
            // pvPut(commandGetDevId);
//-----------

            /* at the end of initialization, we get and keep channels current active input type(only for the 3 on/off buttons on OPI). After we find a change, process it
             * and keep the new on */
            %%intypeTracker.channel_1  = seq_pvGet(ssId, pVar->channel1IntypeBitsIndex, SYNC);
            %%intypeTracker.channel_2  = seq_pvGet(ssId, pVar->channel2IntypeBitsIndex, SYNC);
            %%intypeTracker.channel_3  = seq_pvGet(ssId, pVar->channel3IntypeBitsIndex, SYNC);
            %%intypeTracker.channel_4  = seq_pvGet(ssId, pVar->channel4IntypeBitsIndex, SYNC);
            %%intypeTracker.channel_5  = seq_pvGet(ssId, pVar->channel5IntypeBitsIndex, SYNC);
            %%intypeTracker.channel_6  = seq_pvGet(ssId, pVar->channel6IntypeBitsIndex, SYNC);
            %%intypeTracker.channel_7  = seq_pvGet(ssId, pVar->channel7IntypeBitsIndex, SYNC);
            %%intypeTracker.channel_8  = seq_pvGet(ssId, pVar->channel8IntypeBitsIndex, SYNC);

        } state waitCommands
    }

    state waitCommands 
    {
        when(commandGetDevId)
        {
            connected = 1;

            scanEnableDisable = 1;
            pvPut(scanEnableDisable);
            %%wait_1second();

            %%pVar->writeStatus = ls240_queryDeviceID(ssId, pVar); 
            pvPut(output);
            if (writeStatus == 0) 
            {
                pvPut(input);
            }

            scanEnableDisable = 0;
            pvPut(scanEnableDisable);

            commandGetDevId = 0;
            pvPut(commandGetDevId);

        } state commandDone

        when(commandDisconProfibus)
        {
            connected = 1;
            scanEnableDisable = 1;
            pvPut(scanEnableDisable);
            %%wait_1second();

            %%pVar->writeStatus = ls240_setAddr(0, ssId, pVar);

            pvPut(output);
            if (writeStatus == 0) 
            {
                pvPut(input);
            }

            scanEnableDisable = 0;
            pvPut(scanEnableDisable);                       

            commandDisconProfibus = 0;
            pvPut(commandDisconProfibus);
        } state commandDone

        when(commandSetProfibusAddr)
        {
            connected = 1;
            scanEnableDisable = 1;
            pvPut(scanEnableDisable);
            pvGet(profibusAddress);
            %%wait_1second();

            %%pVar->writeStatus = ls240_setAddr(pVar->commandSetProfibusAddr, ssId, pVar);

            pvPut(output);
            if (writeStatus == 0) 
            {
                pvPut(input);
            }

            scanEnableDisable = 0;
            pvPut(scanEnableDisable);                       

            commandSetProfibusAddr = 0;
            pvPut(commandSetProfibusAddr);

            // profibusAddress = 0;        
            // pvPut(profibusAddress);

        } state commandDone

        when(commandListCurves)
        {
            connected = 1;
            %%snprintf(pVar->ssStatusMessage, 40, "%s", "List Curve Command Excuting......\n");
            pvPut(ssStatusMessage);
            
            // sync the actions with EPICS
            scanEnableDisable = 1;
            pvPut(scanEnableDisable);
            %%wait_1second();

            %%pVar->writeStatus = list_curveFiles(pVar->commandListCurves, ssId, pVar);  // first argu is channel number
            if(!writeStatus)
                pvPut(input);

            pvPut(curveFilesArray);
                                  
            scanEnableDisable = 0;
            pvPut(scanEnableDisable);     
            // Keep this, so we can check that the operator/user really listed all avaiable curve files before it issue upload curve command
            // NO!!! cmd must be turned off, otherwise will not be stuck here
            commandListCurves = 0;
            pvPut(commandListCurves);

            // profibusAddress = 0;        
            // pvPut(profibusAddress);

        } state commandDone

        when(commandUploadCurve)
        {
            //pvGet(curveFileToUpload);
            //%%snprintf(pVar->ssStatusMessage, 40, "%s%s", "Uploading Curve: ", pVar->curveFileToUpload);
            %%printf("command upload curve received = %d", pVar->commandUploadCurve);
            %%snprintf(pVar->ssStatusMessage, 40, "%s", "Uploading Curve.... ");
            pvPut(ssStatusMessage);
            connected = 1;
            // sync the actions with EPICS
            scanEnableDisable = 1;
            pvPut(scanEnableDisable);
            // get the file sequence number of the curve file from operator
            pvGet(curveFileSequenceNum);
            //pvGet(curveFileToUpload);
            
            %%wait_1second();

            // TO DO: We need to temperorarily disconnect the PROFIBUS before upload the curve
            
            // TO DO: First we need to remember the current PROFIBUS address, so we can restore it after curve uploading

            // Put Profibus on a power on restet state by set the adddress to 126

            %%pVar->writeStatus = upload_curve(ssId, pVar);

            pvPut(output);

            if (writeStatus == 0) 
            {
                pvPut(input);
            }

            // TO DO: Restore and bring back the PROFIBUS connection


            scanEnableDisable = 0;
            pvPut(scanEnableDisable);                       

            commandUploadCurve = 0;
            pvPut(commandUploadCurve);

            // %%strncpy(pVar->curveFileToUpload, "", 1);        
            // pvPut(curveFileToUpload);

        } state commandDone

        when (onoffCMD)
        {
            // first sync with monitor      TODO:     !!!

            //call function to process the command
            %%pVar->writeStatus = do_3onoff_process(pVar->onoffCMD, ssId, pVar);

            // update the on/off tracker
           %%pVar->writeStatus = update_onoff_tracker(pVar->onoffCMD, ssId, pVar);
    
            // clean cmd
            onoffCMD = 0;

            // sync with the monitor   TODO: !!!!

        } state commandDone

        when()
        {
           //// %%printf("DEBUG::: Waiting for command... command = %d\n", pVar->commandGetDevId);
           // We check here if the 3 on/off butten have changed state 0 or 1
            if(intypeTracker.channel_1 != channel1IntypeBits)
                onoffCMD = 1;
            else if(intypeTracker.channel_2 != channel2IntypeBits)
                onoffCMD = 2;
            else if(intypeTracker.channel_3 != channel3IntypeBits)
                onoffCMD = 3;
            else if(intypeTracker.channel_4 != channel4IntypeBits)
                onoffCMD = 4;
            else if(intypeTracker.channel_5 != channel5IntypeBits)
                onoffCMD = 5;
            else if(intypeTracker.channel_6 != channel6IntypeBits)
                onoffCMD = 6;
            else if(intypeTracker.channel_7 != channel7IntypeBits)
                onoffCMD = 7;
            else if(intypeTracker.channel_8 != channel8IntypeBits)
                onoffCMD = 8;
        } state waitCommands

    }

// ------------------------

    state processCommands 
    {
        when(connected) 
        {
            scanEnableDisable = 1;
            pvPut(scanEnableDisable);
            %%wait_1second();
            %%pVar->writeStatus = ls240_queryDeviceID(ssId, pVar); 
            pvPut(output);
            if (writeStatus == 0) 
            {
                pvPut(input);
            }
            scanEnableDisable = 0;
            pvPut(scanEnableDisable);
            //%%wait_10ms();
           //} state processCommands
        } state commandDone

        when(!connected) 
        {
        } state waitCommands    // waitConnect, this waitConnect state does not exist, it will cause strange linker error!!!!
 
    }

    state commandDone
    {
        when()
        {

        } state waitCommands
    }

}

//---------------------------------------

%{

const char *serial_port = "/dev/tty.SLAB_USBtoUART";      // can not be "const"
const char *curve_dir = "/Users/juntongliu/epics/epics-apps/IOC_LS240Curve/curve/";
char receive_buff[256];
/**
 * @brief  Free the heap memory
 * @note   
 * @param  *pnode: 
 * @retval None
 */
void freeNodes(void *pnode)
{
    free(pnode);
}
#include "ls240Control.st"
/**
 * @brief  Wait 10 millioseconds
 * @note   
 * @retval 
 */
// static int wait_10ms(void)
// {
//     struct timespec tim, tim2;
//     tim.tv_sec = 0;
//     tim.tv_nsec = 1e+7;                 // 10ms  milliom second, 1000,000,000 nanoseconds = 1 second
//     return nanosleep(&tim, &tim2);
// }


/**
 * @brief  Query the device ID (for test scan enable/disable only) 
 * @note   
 * @param  ssId: 
 * @param  *pVar: 
 * @retval 
 */
static int ls240_queryDeviceID(SS_ID ssId, struct UserVar *pVar)
{
   ssize_t rw_ret;
   char strCMD[8];
   snprintf(strCMD, 8, "%s", "*IDN?\n");
   tcflush(pVar->com_fd, TCIOFLUSH);
   rw_ret = write(pVar->com_fd, strCMD, 6);
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(pVar->com_fd);
      pVar->com_fd = -1;
      return -1;
   }
   
    strncpy(pVar->output, strCMD, strlen(strCMD));
    seq_pvPut(ssId, pVar->outputIndex, SYNC);

    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
    dprintf("Device ID query, cmd=%s\n", "*IDN?");
    // get device reply
    if(ls240_getReply(ssId, pVar) < 0)
    {
        printf("Error reading device reply\n");
    }
    else { 
        printf("\nState Machine gets Device ID is: %s\n", pVar->input);
    }
    
    strncpy(pVar->testDeviceID, receive_buff, 40);
    seq_pvPut(ssId, pVar->testDeviceIDIndex, SYNC);
    memset(receive_buff, 0, sizeof(receive_buff));

   return 0;
}

//const char *curve_dir = "/Users/juntongliu/epics/epics-apps/IOC_LS240Curve/curve/";
/**
 * @brief  find all curve files in the curve/ directory 
 * @note   
 * @retval 
 */
static int list_curveFiles(int inNum, SS_ID ssId, struct UserVar *pVar)
{
    DIR *dp = NULL;
    struct dirent *dptr = NULL;
    size_t nameLen;

    // Open the directory
    if(!(dp = opendir(curve_dir)))
    {
        printf("Error open directory: %s!\n", curve_dir);
        perror("Error open directory!");
        //exit(1);
        return -1;
    }

    // Read the contents of the dir and find out all the curve file and store them in an array to send to opi
    int i = 0;
    while((dptr = readdir(dp)))
    {
        // skip the . and ..
        nameLen = strlen(dptr->d_name);
        if(nameLen == 1 && dptr->d_name[0] == '.')
            continue;
        else if(nameLen == 2 && dptr->d_name[0] == '.' && dptr->d_name[1] == '.')
            continue;
        else if(dptr->d_type == DT_DIR || dptr->d_type == DT_LNK)  // skip a sub dir and soft link
            continue;   
        else if(strstr(dptr->d_name, ".340"))                      // a .340 curve file
        {
            dprintf("DEBUG::: the d_name = \'%s\'; length = %d\n", dptr->d_name, strlen(dptr->d_name));

            snprintf(pVar->curveFilesArray[i], 40, "%d%c%c%s", i+1, '.', ' ', dptr->d_name);   // EPICS string length = 40
            i++;
            if(i > MAXNUMCURVEFILE)
            {
                printf("Number of curve files > %d !\n", MAXNUMCURVEFILE);
                break;
            }
        }
        else if(dptr->d_type == DT_REG)                            // a test curve without surfix
        {
            dprintf("DEBUG::: the d_name = \'%s\'; length = %d\n", dptr->d_name, strlen(dptr->d_name));
            snprintf(pVar->curveFilesArray[i], 40, "%d%c%c%s", i+1, '.', ' ', dptr->d_name);   // EPICS string length = 40
            i++;
            if(i > MAXNUMCURVEFILE)
            {
                printf("Number of curve files > %d !\n", MAXNUMCURVEFILE);
                break;
            }
        }
        else{
            printf("Unkonw file type!!");
            continue;
        }
    }

    closedir(dp);

    return 0;
}

/**
 * @brief  Query PROFIBUS address; 1 - 126
 * @note   
 * @retval 
 */
static int ls240_queryADDR(SS_ID ssId, struct UserVar *pVar)
{
    int rw_ret;
    //int tries;
    char queryAddrCMD[16];
    snprintf(queryAddrCMD, 7, "%s%c", "ADDR?", '\n');
    rw_ret = write(pVar->com_fd, queryAddrCMD, strlen(queryAddrCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(pVar->com_fd);
        return -1;
    }
    DBGPRINT("write(): %d bytes data sent to device 240\n", (int)rw_ret);
    dprintf("Query device PROFIBUS address command=%s\n", queryAddrCMD);
    // get device reply
    if (ls240_getReply(ssId, pVar) < 0)
        printf("Error Reading device reply!\n");
    else {
        printf("\nPROFIBUS address is: %s\n", receive_buff);
        snprintf(pVar->ssStatusMessage, 40, "%s%s", "Profibus address is: ", receive_buff);
        seq_pvPut(ssId, pVar->ssStatusMsgIndex, SYNC);
    }
   bzero(receive_buff, sizeof(receive_buff)); // memset(rcv_buff, 0, BUFFLEN);

   return 0;
}

/**
 *  Set PROFIBUS address:  1 - 126. Address 126 indicate that the address is not configured and 
 *  can be set by a PROFIBUS master.
 *  When set the PROFIBUS address to 126, the PROFIBUS enter into state   0 --- "POWER ON/RESET", 
 *  When set the PROFIBUS address to 1-125, the PROFIBUS enter into state 1 --- "Waiting for parameterization"
 *  2 = Waiting for configuration,   3 = Data exchange
 */
unsigned short profibusAddrForRestore;
static int ls240_setAddr(int inNum, SS_ID ssId, struct UserVar *pVar)
{

    char setCMD[32];
    int rw_ret;
    unsigned short addr;

    //    addr = atoi(input);
    seq_pvGet(ssId, pVar->originalProfibusAddrIndex, SYNC);  // for later restore
    profibusAddrForRestore = pVar->originalProfibusAddr;

    seq_pvGet(ssId, pVar->profibusAddressIndex, SYNC);
    if(pVar->commandDisconProfibus)
        addr = 126;                         // operator clicked on "Disconnect Profitbus" button
    else if(pVar->commandSetProfibusAddr)
        addr = pVar-> profibusAddress;      // pVar->commandDisconProfibus;
    else{
        printf("In ls240_setAddr(): address is unclear!\n");
        return -1;
    }

    if(addr < 1 || addr > 126)
    {
        snprintf(pVar->ssStatusMessage, 40, "%s", "Wrong PROFIBUS address. It must between 1 - 126\n");
        seq_pvPut(ssId, pVar->ssStatusMsgIndex, SYNC);
        return -1;
    }

    snprintf(setCMD, 32, "%s%c%d%c", "ADDR", ' ', addr, '\n');

    rw_ret = write(pVar->com_fd, setCMD, strlen(setCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(pVar->com_fd);
        pVar->com_fd = -1;
        return -1;
    }
    printf("\nPROFIBUS address has been set to: %d\n\n", addr);

    wait_10ms();   
    ls240_queryADDR(ssId, pVar);

    if(pVar->commandSetProfibusAddr)
    {
        pVar->originalProfibusAddr = addr;                   // put new addr to OPI
        seq_pvPut(ssId, pVar->originalProfibusAddrIndex, SYNC);
    }
    else if(pVar->commandDisconProfibus)
    {
        pVar->profibusAddress = 0;
        seq_pvPut(ssId, pVar->profibusAddressIndex, SYNC);
        pVar->originalProfibusAddr = 126;
        seq_pvPut(ssId, pVar->originalProfibusAddrIndex, SYNC);
    }
    snprintf(pVar->ssStatusMessage, 40, "\nPROFIBUS address has been set to: %d\n\n", addr);
    seq_pvPut(ssId, pVar->ssStatusMsgIndex, SYNC);

    return 0;
}

static int compare_onoff_3fields(int inNum, SS_ID ssId, struct UserVar *pVar, char **tokens)
{ 
    unsigned int activeChBit, activeAutoBit, activeCurrentBit;
    unsigned int newChBit, newAutoBit, newCurrentBit;

    
    if(inNum == 1)
    {
        // Check the channel on/off field: last part/bit 
        newChBit = pVar->channel1IntypeBits & 0x01;
        // token 6 is for channel on/off
        activeChBit = atoi(tokens[5]);

        // check the autorange field
        newAutoBit = pVar->channel1IntypeBits & 0x02;
        // token 2 is for auto range
        activeAutoBit = atoi(tokens[1]);
        
        // check the current reverse field
        newCurrentBit = pVar->channel1IntypeBits & 0x04;
        // token 4 is for current reverse
        activeCurrentBit = atoi(tokens[3]);
        
    }
    else if(inNum == 2)
    {
        // Check the channel on/off field: last part/bit 
        newChBit = pVar->channel2IntypeBits & 0x01;
        // token 6 is for channel on/off
        activeChBit = atoi(tokens[5]);
        
        // check the autorange field
        newAutoBit = pVar->channel2IntypeBits & 0x02;
        // token 2 is for auto range
        activeAutoBit = atoi(tokens[1]);
        
        // check the current reverse field
        newCurrentBit = pVar->channel2IntypeBits & 0x04;
        // token 4 is for current reverse
        activeCurrentBit = atoi(tokens[3]);
        
    }
    else if(inNum == 3)
    {
        // Check the channel on/off field: last part/bit 
        newChBit = pVar->channel3IntypeBits & 0x01;
        // token 6 is for channel on/off
        activeChBit = atoi(tokens[5]);
        
        // check the autorange field
        newAutoBit = pVar->channel3IntypeBits & 0x02;
        // token 2 is for auto range
        activeAutoBit = atoi(tokens[1]);
        
        // check the current reverse field
        newCurrentBit = pVar->channel3IntypeBits & 0x04;
        // token 4 is for current reverse
        activeCurrentBit = atoi(tokens[3]);
        
    }
    else if(inNum == 4)
    {
        // Check the channel on/off field: last part/bit 
        newChBit = pVar->channel4IntypeBits & 0x01;
        // token 6 is for channel on/off
        activeChBit = atoi(tokens[5]);
        
        // check the autorange field
        newAutoBit = pVar->channel4IntypeBits & 0x02;
        // token 2 is for auto range
        activeAutoBit = atoi(tokens[1]);
        
        // check the current reverse field
        newCurrentBit = pVar->channel4IntypeBits & 0x04;
        // token 4 is for current reverse
        activeCurrentBit = atoi(tokens[3]);
        
    }
    else if(inNum == 5)
    {
        // Check the channel on/off field: last part/bit 
        newChBit = pVar->channel5IntypeBits & 0x01;
        // token 6 is for channel on/off
        activeChBit = atoi(tokens[5]);
        
        // check the autorange field
        newAutoBit = pVar->channel5IntypeBits & 0x02;
        // token 2 is for auto range
        activeAutoBit = atoi(tokens[1]);
        
        // check the current reverse field
        newCurrentBit = pVar->channel5IntypeBits & 0x04;
        // token 4 is for current reverse
        activeCurrentBit = atoi(tokens[3]);
        
    }
    else if(inNum == 6)
    {
        // Check the channel on/off field: last part/bit 
        newChBit = pVar->channel6IntypeBits & 0x01;
        // token 6 is for channel on/off
        activeChBit = atoi(tokens[5]);
        
        // check the autorange field
        newAutoBit = pVar->channel6IntypeBits & 0x02;
        // token 2 is for auto range
        activeAutoBit = atoi(tokens[1]);
        
        // check the current reverse field
        newCurrentBit = pVar->channel6IntypeBits & 0x04;
        // token 4 is for current reverse
        activeCurrentBit = atoi(tokens[3]);
        
    }
    else if(inNum == 7)
    {
        // Check the channel on/off field: last part/bit 
        newChBit = pVar->channel7IntypeBits & 0x01;
        // token 6 is for channel on/off
        activeChBit = atoi(tokens[5]);
        
        // check the autorange field
        newAutoBit = pVar->channel7IntypeBits & 0x02;
        // token 2 is for auto range
        activeAutoBit = atoi(tokens[1]);
        
        // check the current reverse field
        newCurrentBit = pVar->channel7IntypeBits & 0x04;
        // token 4 is for current reverse
        activeCurrentBit = atoi(tokens[3]);
        
    }
    else if(inNum == 8)
    {
        // Check the channel on/off field: last part/bit 
        newChBit = pVar->channel8IntypeBits & 0x01;
        // token 6 is for channel on/off
        activeChBit = atoi(tokens[5]);
        
        // check the autorange field
        newAutoBit = pVar->channel8IntypeBits & 0x02;
        // token 2 is for auto range
        activeAutoBit = atoi(tokens[1]);
        
        // check the current reverse field
        newCurrentBit = pVar->channel8IntypeBits & 0x04;
        // token 4 is for current reverse
        activeCurrentBit = atoi(tokens[3]);
        
    }
    else{
        printf("Wrong channel number!\n");
        return -1;
    }

    if(newChBit != activeChBit)
        onoff.channelOnOff = newChBit;
    else
        onoff.channelOnOff = activeChBit;

    if(newAutoBit != activeAutoBit)
        onoff.autorange = newAutoBit;
    else
        onoff.autorange = activeAutoBit;

    if(newCurrentBit != activeCurrentBit)
        onoff.currentReverse = newCurrentBit;
    else
        onoff.currentReverse = activeCurrentBit;      


    return 0;
}

/**
 * @brief  Do 3 On/Off buttons process for 8 channels
 * @note   
 * @param  inNum: 
 * @param  ssId: 
 * @param  *pVar: 
 * @retval 
 */
static int do_3onoff_process(int inNum, SS_ID ssId, struct UserVar *pVar)
{
    char *tokens[8];
    char *cp;
    const char *delim = " ,\n\r";
    int numTokens, rw_ret;
    char setCMD[64];
    char inType[64];
    char buff[64];

    bzero(inType, sizeof(inType));

    // read the channel's current active input type setting
    if(ls240_queryIntype(inNum, inType, ssId, pVar) < 0)
    {
        printf("Error query input type for channel %d\n", inNum);
        return -1;
    }

    // now we decide if any of the 3 on/off buttons states have been changed by operator
    snprintf(buff, sizeof(buff), "%s%c%c", receive_buff, '\n', '\0');
    cp = buff;
    numTokens = pickOutToken(tokens, cp, delim);
    if(numTokens != 6)
    {
        printf("Error received from intype query!\n");
        return -1;
    }

    // compare the 3 fields
    compare_onoff_3fields(inNum, ssId, pVar, tokens);

    // send to ls240
    snprintf(setCMD, sizeof(setCMD), "%s%c%d%c%d%c%d%c%d%c%d%c%d%c%d%c", "INTYPE", ' ', inNum, ',', 
                atoi(tokens[0]), ',', onoff.autorange, ',', atoi(tokens[2]), ',', onoff.currentReverse, ',', atoi(tokens[4]), ',', onoff.channelOnOff, '\n');
        
    dprintf("Set input type cmd = %s\n", setCMD);
    rw_ret = write(com_fd, setCMD, strlen(setCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240 for set new input type!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

    // new input type has write to ls240, now update opi
    switch(inNum)
    {
        case 1:
            snprintf(pVar->channel1InType, 40, "%d%c%d%c%d%c%d%c%d%c%d", atoi(tokens[0]), ',', onoff.autorange, ',', atoi(tokens[2]), ',',
                onoff.currentReverse, ',', atoi(tokens[4]), ',', onoff.channelOnOff);
            seq_pvPut(ssId, pVar->channel1InTypeIndex, SYNC);
            break;
        case 2:
            snprintf(pVar->channel2InType, 40, "%d%c%d%c%d%c%d%c%d%c%d", atoi(tokens[0]), ',', onoff.autorange, ',', atoi(tokens[2]), ',',
                onoff.currentReverse, ',', atoi(tokens[4]), ',', onoff.channelOnOff);
            seq_pvPut(ssId, pVar->channel2InTypeIndex, SYNC);
            break;
        case 3:
            snprintf(pVar->channel3InType, 40, "%d%c%d%c%d%c%d%c%d%c%d", atoi(tokens[0]), ',', onoff.autorange, ',', atoi(tokens[2]), ',',
                onoff.currentReverse, ',', atoi(tokens[4]), ',', onoff.channelOnOff);
            seq_pvPut(ssId, pVar->channel3InTypeIndex, SYNC);
            break;        
        case 4:
            snprintf(pVar->channel4InType, 40, "%d%c%d%c%d%c%d%c%d%c%d", atoi(tokens[0]), ',', onoff.autorange, ',', atoi(tokens[2]), ',',
                onoff.currentReverse, ',', atoi(tokens[4]), ',', onoff.channelOnOff);
            seq_pvPut(ssId, pVar->channel4InTypeIndex, SYNC);
            break;
        case 5:
            snprintf(pVar->channel5InType, 40, "%d%c%d%c%d%c%d%c%d%c%d", atoi(tokens[0]), ',', onoff.autorange, ',', atoi(tokens[2]), ',',
                onoff.currentReverse, ',', atoi(tokens[4]), ',', onoff.channelOnOff);
            seq_pvPut(ssId, pVar->channel5InTypeIndex, SYNC);
            break;
        case 6:
            snprintf(pVar->channel6InType, 40, "%d%c%d%c%d%c%d%c%d%c%d", atoi(tokens[0]), ',', onoff.autorange, ',', atoi(tokens[2]), ',',
                onoff.currentReverse, ',', atoi(tokens[4]), ',', onoff.channelOnOff);
            seq_pvPut(ssId, pVar->channel6InTypeIndex, SYNC);
            break;
        case 7:
            snprintf(pVar->channel7InType, 40, "%d%c%d%c%d%c%d%c%d%c%d", atoi(tokens[0]), ',', onoff.autorange, ',', atoi(tokens[2]), ',',
                onoff.currentReverse, ',', atoi(tokens[4]), ',', onoff.channelOnOff);
            seq_pvPut(ssId, pVar->channel7InTypeIndex, SYNC);
            break;
        case 8:
            snprintf(pVar->channel8InType, 40, "%d%c%d%c%d%c%d%c%d%c%d", atoi(tokens[0]), ',', onoff.autorange, ',', atoi(tokens[2]), ',',
                onoff.currentReverse, ',', atoi(tokens[4]), ',', onoff.channelOnOff);
            seq_pvPut(ssId, pVar->channel8InTypeIndex, SYNC);
            break;
        default:
            printf("Wrong input number!\n");
            return -1;                        
    }

    return 0;

}

/**
 * @brief  After process a command issued by operator with the click on the 3 on/off button of the OPI,
 *         this function is called to update the tracker value.
 * @note   
 * @param  inNum: 
 * @param  ssId: 
 * @param  *pVar: 
 * @retval 
 */
static int update_onoff_tracker(int inNum, SS_ID ssId, struct UserVar *pVar)
{
    switch (inNum)
    {
        case 1:
            intypeTracker.channel_1 = pVar->channel1IntypeBits;
            // pVar->channel1IntypeFlag = 0;                      // <<<========== TODO : check here more. No need to set 0, monitored variable
            break;
        case 2:
            intypeTracker.channel_2 = pVar->channel2IntypeBits;
            // pVar->channel2IntypeFlag = 0;
            break;
        case 3:
            intypeTracker.channel_3 = pVar->channel3IntypeBits;
            // pVar->channel3IntypeFlag = 0;
            break;
        case 4:
            intypeTracker.channel_4 = pVar->channel4IntypeBits;
            // pVar->channel4IntypeFlag = 0;
            break;
        case 5:
            intypeTracker.channel_5 = pVar->channel5IntypeBits;
            // pVar->channel5IntypeFlag = 0;
            break;
        case 6:
            intypeTracker.channel_6 = pVar->channel6IntypeBits;
            // pVar->channel6IntypeFlag = 0;
            break;
        case 7:
            intypeTracker.channel_7 = pVar->channel7IntypeBits;
            // pVar->channel7IntypeFlag = 0;
            break;
        case 8:
            intypeTracker.channel_8 = pVar->channel8IntypeBits;
            // pVar->channel8IntypeFlag = 0;
            break;
        default:
            printf("Wrong channel number!\n");
            return -1;
    }
    return 0;
}




}%
