
// ------- With Asyn -------

program ls240Curve

//program ls240Curve("P=ls240Curve:=, PORT=L0")

option +r;

%%#include <stdio.h>
%%#include <stdlib.h>
%%#include <string.h>
%%#include <termios.h>
%%#include <unistd.h>
%%#include <stdbool.h>
%%#include <cantProceed.h>
%%#include <ctype.h>
%%#include <sys/stat.h>
%%#include <fcntl.h>
%%#include <sys/signal.h>
%%#include <sys/types.h>
//#include <sys/stat.h>
//#include <linux/kernel.h>
%%#include <termios.h>
%%#include <stdarg.h>
%%#include <time.h>      // for nanosleep()
%%#include <stdbool.h>
%%#include <dirent.h>
%%#include <ellLib.h> 
%%#include <epicsString.h>
%%#include <epicsEvent.h>
%%#include <asynDriver.h>
%%#include <asynOctet.h>
%%#include <asynOctetSyncIO.h>
%%#include "ls240Curve.h"

// pvs
string input;  assign input     to "{P}:calibrationInput";
string output; assign output    to "{P}:calibrationOutput";
int connected; assign connected to "{P}:connected";
//string adjuOutput; assign adjuOutput to "{P}calibrationAdjuOutput";

// Variables
char *listenerPortName;
//char *IOPortName;
string IOPortName;
string asynPortName;
int readStatus;
int writeStatus;
char *pasynUser;    /* This is really asynUser* */
char *registrarPvt; /* This is really void* */
char *eventId;      /* Thus is really epicsEventId */
unsigned int idex;  /* PV index, initialize at init phase, can not use VAR_ID */
unsigned int odex;
unsigned int aodex;

// Function prototype
/*
%%static int pickOutToken(char **token, char *cp, const char *delimiter);
//%%static int init_serial_ports(void);
%%static int wait_10ms(void);
%%static int ls240_getReply(void);
%%static int parse_curve(char *fileName);
%%static int print_parse_result(void);
%%static int ls240_queryIntype(int inNum, char *inputType);
%%static int ls240_queryCRVPT(int inNum, int index);
%%static int ls240_queryCRVHDR(char *crvHeader, int input);
%%static int ls240_dumpSaveCurve(char *fileName, int inNum);
%%static int upload_curve(int inNum);
%%static int list_curveFiles(void);
*/
%%static void connectionCallback(void *drvPvt, asynUser *pasynUserIn, char *portName, size_t len, int eomReason);
%%static void initialize(SS_ID ssId, struct UserVar *pVar);
%%static int readSocketCom(SS_ID ssId, struct UserVar *pVar);
%%static int writeSocketCom(SS_ID ssId, struct UserVar *pVar);
//%%static int ls240_queryDeviceID(void);
//%%static int ls240_getReply(void);
%%static int wait_10ms(void);

void freeNodes(void *pnode)
{
    free(pnode);
}


ss ls240Curve
{
   state init 
    {
        when() 
        {
            //listenerPortName = macValueGet("PORT");
            //asynPortName = macValueGet("PORT");   
            strncpy(asynPortName, "L0", 2);
            strncpy(IOPortName, "L0", 2);
            %%printf("DEBUG::: 1 Initial ------------\n");
            %%initialize(ssId, pVar);
            connected = 0;
            idex = pvIndex(input);
            odex = pvIndex(output);
	        //aodex = pvIndex(adjuOutput);
        } state waitConnect
    }

    state waitConnect 
    {
        when() 
        {
            connected = 1;    //
            pvPut(connected);
            %%printf("DEBUG::: 3 waitConnect --Here will be wait for upload command from OPI -- connected = %d --------\n", pVar->connected);
            //%%epicsEventWait((epicsEventId)pVar->eventId);
            pvPut(connected);
            // Before go to process upload, here need set the pv to disable all records scan to let upload function take the 
            // serial link.
        } state processCommands
    }

    state processCommands 
    {
        when(connected) 
        {
            //  %%pVar->readStatus = readSocketCom(ssId, pVar); 
	        // //readStatus = readSocketCom(ssId, pVar, pvIndex(input));
            // if (readStatus == 0) 
            // {
            //     pvPut(input);
	        //  /* %%calib_process_measurement(ssId, pVar);  
            //     strcpy(output, "OK");
            //     strcpy(output, input);      
            //     strncat(input, "received OK\n", sizeof(input)-strlen(input)-1);*/
		    //     strncpy(output, input, sizeof(output));
            //     %%pVar->writeStatus = writeSocketCom(ssId, pVar);
            %%printf("DEBUG::: 4 processComands ------------\n");
            strcpy(output, "*IDN?\n");
            pvPut(output);
            %%pVar->writeStatus = writeSocketCom(ssId, pVar);
            if (writeStatus == 0) 
            {
                pvPut(output);
            }
            %%wait_10ms();
            %%pVar->readStatus = readSocketCom(ssId, pVar); 
            if (readStatus == 0) 
            {
                pvPut(input);
            }
            pvPut(input);
            //}
        //} state processCommands
        } state done

        when(!connected) 
        {
        } state waitConnect
 
    }

    state done
    {
        when()
        {

        } state done
    }

}

%{

/* JT, call back function. asynOctet->registerInterruptUser() will be called to register this callback function to the asynOctet
 * interface. */
static void connectionCallback(void *drvPvt, asynUser *pasynUserIn, char *portName, size_t len, int eomReason)
{
    struct UserVar  *pVar = (struct UserVar *)drvPvt;
    asynUser *pasynUser;
    asynStatus status;
    
    //pVar->IOPortName = epicsStrDup(portName);
    strcpy(pVar->IOPortName, "L0");
    status = pasynOctetSyncIO->connect(portName, 0, &pasynUser, NULL);
    pVar->pasynUser = (char *)pasynUser;
    printf("DEBUG::: connection call back is called! len = %d\n", (int)len);
    //asynPrint(pasynUser, ASYN_TRACE_FLOW, "ipSNCServer: connectionCallback, portName=%s\n", portName);
    asynPrint(pasynUser, ASYN_TRACE_ERROR, "ipSNCServer: connectionCallback, len=%d\n", (int)len);
    if (status) 
    {
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
                  "ipSNCServer:connectionCallback: unable to connect to portName = %s\n",
                  portName);
        return;
    }

    status = pasynOctetSyncIO->setInputEos(pasynUser, "\r\n", 2);
    if (status) 
    {
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
                  "ipSNCServer:connectionCallback: unable to set input EOS on %s: %s\n",
                  portName, pasynUser->errorMessage);
        return;
    }
    status = pasynOctetSyncIO->setOutputEos(pasynUser, "\r\n", 2);
    if (status) 
    {
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
                  "ipSNCServer:connectionCallback: unable to set output EOS on %s: %s\n",
                  portName, pasynUser->errorMessage);
        return;
    }
    /* Send an EPICS event to wake up the SNL program */
    pVar->connected = 1;
    epicsEventSignal( (epicsEventId)pVar->eventId);
}

static int wait_10ms(void)
{
    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = 1e+7;                 // 10ms  milliom second, 1000,000,000 nanoseconds = 1 second
    return nanosleep(&tim, &tim2);
}

static void initialize(SS_ID ssId, struct UserVar *pVar)
{
    int addr = 0;
    asynInterface *pasynInterface;
    asynUser *pasynUser;
    asynOctet *pasynOctet;
    void *registrarPvt;
    int status;
    int calibStatus;

    pVar->eventId = (char *)epicsEventCreate(epicsEventEmpty);
    
    pasynUser = pasynManager->createAsynUser(0, 0);
    pVar->pasynUser = (char *)pasynUser;
    pasynUser->userPvt = pVar;
    
    status = pasynManager->connectDevice(pasynUser, pVar->asynPortName, addr);   // pVar->listenerPortName, addr);
    if(status != asynSuccess) 
    {
        printf("initialize(): can't connect to port %s: %s\n", pVar->asynPortName, pasynUser->errorMessage); // pVar->listenerPortName, pasynUser->errorMessage);
        return;
    }

    pasynInterface = pasynManager->findInterface(pasynUser, asynOctetType, 1);
    if(!pasynInterface) 
    {
        printf("%s driver not supported\n", asynOctetType);
        return;
    }
    
    pasynOctet = (asynOctet *)pasynInterface->pinterface;

    // For IP, this is OK, but for serial with ls240, there are queries for temperatures all the time. SNC program here are not interested in those
    // traffic!!!   
    status = pasynOctet->registerInterruptUser(
                 pasynInterface->drvPvt, pasynUser,
                 connectionCallback, pVar, &registrarPvt);

    pVar->registrarPvt = registrarPvt;
    if(status != asynSuccess) 
    {
        printf("ipSNCServer devAsynOctet registerInterruptUser %s\n",
               pasynUser->errorMessage);
    }

    // JT, we test here
    printf("DEBUG:::2, asynPortName=%s  addr=%d ----------------------\n", pVar->asynPortName, addr);
    //pVar->connected = 1;
    //pvPut(pVar->connected);

}


// static int ls240_queryDeviceID(void)
// {
//    ssize_t rw_ret;
//    tcflush(com_fd, TCIOFLUSH);
//    rw_ret = write(com_fd, "*IDN?\n", 6);
//    if (rw_ret < 0)
//    {
//       perror("Error write to 240!\n");
//       close(com_fd);
//       com_fd = -1;
//       return -1;
//    }
//    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
//    dprintf("Device ID query, cmd=%s\n", "*IDN?");
   // get device reply
//    if(ls240_getReply() < 0)
//    {
//       printf("Error reading device reply\n");
//    }
//    else
//       printf("\nDevice ID is: %s\n", rcv_buff);
//    memset(rcv_buff, 0, BUFFLEN);

//    return 0;
// }

const char *curve_dir = "/Users/juntongliu/epics/epics-apps/IOC_LS240Curve/curve/";
/**
 * @brief  find all curve files in the curve/ directory 
 * @note   
 * @retval 
 */
static int list_curveFile(void)
{
    DIR *dp = NULL;
    struct dirent *dptr = NULL;
    //char buff[256];      // buffer for storing the directory path
    //memset(buff, 0, sizeof(buff));

    //strncpy(buff, argv[1], strlen(curve_dir));
    // Open the directory
    if(!(dp = opendir(curve_dir)))
    {
        printf("Error open directory: %s!\n", curve_dir);
        //exit(1);
        return -1;
    }

    // Read the contents of the dir
    while((dptr = readdir(dp)))
    {
        // skip the . and ..
        if(strlen(dptr->d_name) == 1 && dptr->d_name[0] == '.')
            continue;
        else if(strlen(dptr->d_name) == 2 && dptr->d_name[0] == '.' && dptr->d_name[1] == '.')
            continue;
        else if(strstr(dptr->d_name, ".340"))
        {

        }
    }

    return 0;
}

// Receive data from device through Asyn
static int readSocketCom(SS_ID ssId, struct UserVar *pVar)
{
    char buffer[BUFFLEN];
    size_t nread;
    int eomReason;
    asynUser *pasynUser = (asynUser *)pVar->pasynUser;
    asynStatus status;

    status = pasynOctetSyncIO->read(pasynUser, buffer, BUFFLEN,
                                    -1.0, &nread, &eomReason);
    if (status) {
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
                  "ipSNCServer:readSocketCom: read error on: %s: %s\n",
                  pVar->asynPortName, pasynUser->errorMessage);
        pVar->connected = 0;
        strcpy(pVar->input, "");
    }
    else {
        asynPrint(pasynUser, ASYN_TRACEIO_DEVICE,
                  "ipSNCServer:readSocketCom: %s read %s\n",
                   pVar->asynPortName, buffer);
        printf("\n===>>> The receive_buffer = %s", buffer);
        strcpy(pVar->input, buffer);
	    //calib_process_measurement(ssId, pVar, buffer);
    }
    return(status);
}

// Send data to device through Asyn
static int writeSocketCom(SS_ID ssId, struct UserVar *pVar)
{
    size_t nwrite;
    asynUser *pasynUser = (asynUser *)pVar->pasynUser;
    asynStatus status;

    printf("DEBUG:::5, in writeSocketCom() --- asynPortName=%s ---- output=%s -------------\n", pVar->asynPortName, pVar->output);
    
    status = pasynOctetSyncIO->write(pasynUser, pVar->output, strlen(pVar->output),
                                     0.0, &nwrite);
    if (status) {
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
                  "ipSNCServer:writeSocketCom: write error on: %s: %s\n",
                  pVar->IOPortName, pasynUser->errorMessage);
        pVar->connected = 0;
    }
    else {
        asynPrint(pasynUser, ASYN_TRACEIO_DEVICE,
                   "ipSNCServer:writeSocketCom: %s write %s\n",
                   pVar->IOPortName, pVar->output);
    }

    return(status);
}


}%
