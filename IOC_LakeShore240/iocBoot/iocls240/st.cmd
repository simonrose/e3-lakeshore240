#!../../bin/darwin-x86/ls240

#- You may have to change curveData to something else
#- everywhere it appears in this file

#< envPaths

# define prototol path
#epicsEnvSet("TOP", "/Users/juntongliu/epics/epics-apps/IOC_Lakeshore240")
#epicsEnvSet("TOP", "/Users/juntongliu/epics/epics-apps/IOC_LS240Curve")
#epicsEnvSet("TOP", "/Users/juntongliu/epics/epics-apps/IOC_LS240Control")
epicsEnvSet("TOP", "/Users/juntongliu/epics/epics-apps/IOC_LS240_IP")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(TOP)/protocol")
# The serial port name need to be decided
epicsEnvSet "TTY" "$(TTY=/dev/tty.SLAB_USBtoUART)"
#epicsEnvSet "P" "$(P=lakeshore240)"
#epicsEnvSet "PORT" "$(PORT=L0)"
#epicsEnvSet "ADDR" "$(ADDR=0)"
## Register all support components
# dbLoadDatabase("../../dbd/curveData.dbd",0,0)
dbLoadDatabase("./dbd/ls240.dbd", 0, 0)
ls240_registerRecordDeviceDriver(pdbbase) 

##########################################################
##################### Set up TTY Port ####################
# ## drvAsynSerialPortConfigure  port ipInfo priority noAutoconnect noProcessEos
# drvAsynSerialPortConfigure("L0", "$(TTY)", 0, 0, 0)
# asynSetOption("L0", -1, "baud", "115200")
# asynSetOption("L0", -1, "bits", "8")
# asynSetOption("L0", -1, "parity", "none")
# asynSetOption("L0", -1, "stop", "1")
# asynSetOption("L0", -1, "clocal", "Y")
# asynSetOption("L0", -1, "crtscts", "N")
# # RTS CTS pins, output hardware flow control
# #asynSetOption("L0", -1, "baud", "9600")
# # ------
# asynOctetSetInputEos("L0", -1, "\n")
# asynOctetSetOutputEos("L0", -1, "\n")

########################################################
#################### Set up IP Port ####################
#drvAsynIPServerPortConfigure("P5002","localhost:5002",1,0,0,0)
drvAsynIPPortConfigure("L0","localhost:5002",1,0,0,0)


#asynSetTraceIOMask("L0", -1, 0x02)
#asynSetTraceMask("L0", -1, 0X9)

## Load record instances
#dbLoadRecords("../../db/curveData.db","user=juntongliu")
#dbLoadRecords("./db/test.db","user=juntongliu, P=lakeshore240, PORT=L0, ADDR=0")
#dbLoadRecords("./db/ls240.db","user=juntongliu, P=lakeshore240, PORT=L0, ADDR=0")
dbLoadRecords("./db/myAsynString.db","user=juntongliu, P=lakeshore240, PORT=L0, ADDR=0")
dbLoadRecords("./db/ls240monitor.db","user=juntongliu, P=lakeshore240, PORT=L0, ADDR=0")
dbLoadRecords("./db/ls240control.db","user=juntongliu, P=lakeshore240, PORT=L0, ADDR=0")

traceIocInit

iocInit()

## Start any sequence programs
#seq snccurveData,"user=juntongliu"
#seq ls240Curve, "user=juntongliu, P=lakeshore240"
seq ls240Control, "user=juntongliu, P=lakeshore240"
#seq sncExample, "user=juntongliu"
