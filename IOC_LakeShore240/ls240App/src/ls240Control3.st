
       // Part of the init_control() function to initial the curve_name, curve_serial, intype of 8 channels at system 
       // start up time. Also, we can make it periodically up-date this inforamtion at running time. 

       //Here, we query the current curve header     <<<====== Now main monitor and Stream has added all this PVs, so no need here init_control()
        if(ls240_queryCRVHDR(channelCurveHeader, inNum, ssId, pVar) < 0)
        {
            printf("In list_curveFile(), Error query current curve header!\n");
            return -1;
        }

        strncpy(curveSerial, receive_buff, 10);                  // first 10 chars in the rcv_buff is serial. max allowed length for curve serial is 10 
        printf("DEBUG::: query CRVHDR get: %s\n", receive_buff);
        switch(inNum)
        {
            case 1:
                strncpy(pVar->channel1CurveSerial, curveSerial, 40);
                seq_pvPut(ssId, pVar->channel1CurveSerialIndex, SYNC);
                break;
            case 2:
                strncpy(pVar->channel2CurveSerial, curveSerial, 40);
                seq_pvPut(ssId, pVar->channel2CurveSerialIndex, SYNC);
                break;
            case 3:
                strncpy(pVar->channel3CurveSerial, curveSerial, 40);
                seq_pvPut(ssId, pVar->channel3CurveSerialIndex, SYNC);
                break;
            case 4:
                strncpy(pVar->channel4CurveSerial, curveSerial, 40);
                seq_pvPut(ssId, pVar->channel4CurveSerialIndex, SYNC);
                break;
            case 5:
                strncpy(pVar->channel5CurveSerial, curveSerial, 40);
                seq_pvPut(ssId, pVar->channel5CurveSerialIndex, SYNC);
                break;
            case 6:
                strncpy(pVar->channel6CurveSerial, curveSerial, 40);
                seq_pvPut(ssId, pVar->channel6CurveSerialIndex, SYNC);
                break;
            case 7:
                strncpy(pVar->channel7CurveSerial, curveSerial, 40);
                seq_pvPut(ssId, pVar->channel7CurveSerialIndex, SYNC);
                break;
            case 8:
                strncpy(pVar->channel8CurveSerial, curveSerial, 40);
                seq_pvPut(ssId, pVar->channel8CurveSerialIndex, SYNC);
                break;
            default:
                printf("In list_curveFile(), Wrong channel number!\n");
                return -1;
        }    
    
    


    char rcv_buff[256];
    char curveSerial[64];
    char channelInType[64];
    const char *delim = " ,\n\r";
    char *tokens[8];
    char *cp;
    unsigned int numTokens;
    unsigned int inFlag = 0;
    unsigned int inFlagBit;
    char *strp;
 
    /*
     * First when operator click the list-curve button, we get the current active curve header and pick out the
     * curve-serial-number and display it on the OPI.
     * we do a read to clean the receive buffer contents left from a previous command issue by monitor block
     */
    //char channelCurveHeader[128];
    /* since we are multi-threads now, so, we must clean the line, in case that the monitor thread send a query and 
     * we get the answer. That will be wrong answer for us.
    */

    read(pVar->com_fd, rcv_buff, 256 - 1);   // can use a tcflush(com_fd, TCIOFLUSH); to fush the line.
    Here, we query the current curve header
    if(ls240_queryCRVHDR(channelCurveHeader, inNum, ssId, pVar) < 0)
    {
        printf("In list_curveFile(), Error query current curve header!\n");
        return -1;
    }

    strncpy(curveSerial, receive_buff, 10);        // max allowed length for curve serial is 10 
    printf("DEBUG::: query CRVHDR get: %s\n", receive_buff);
    switch(inNum)
    {
        case 1:
            strncpy(pVar->channel1CurveSerial, curveSerial, 40);
            seq_pvPut(ssId, pVar->channel1CurveSerialIndex, SYNC);
            break;
        case 2:
            strncpy(pVar->channel2CurveSerial, curveSerial, 40);
            seq_pvPut(ssId, pVar->channel2CurveSerialIndex, SYNC);
            break;
        case 3:
            strncpy(pVar->channel3CurveSerial, curveSerial, 40);
            seq_pvPut(ssId, pVar->channel3CurveSerialIndex, SYNC);
            break;
        case 4:
            strncpy(pVar->channel4CurveSerial, curveSerial, 40);
            seq_pvPut(ssId, pVar->channel4CurveSerialIndex, SYNC);
            break;
        case 5:
            strncpy(pVar->channel5CurveSerial, curveSerial, 40);
            seq_pvPut(ssId, pVar->channel5CurveSerialIndex, SYNC);
            break;
        case 6:
            strncpy(pVar->channel6CurveSerial, curveSerial, 40);
            seq_pvPut(ssId, pVar->channel6CurveSerialIndex, SYNC);
            break;
        case 7:
            strncpy(pVar->channel7CurveSerial, curveSerial, 40);
            seq_pvPut(ssId, pVar->channel7CurveSerialIndex, SYNC);
            break;
        case 8:
            strncpy(pVar->channel8CurveSerial, curveSerial, 40);
            seq_pvPut(ssId, pVar->channel8CurveSerialIndex, SYNC);
            break;
        default:
            printf("In list_curveFile(), Wrong channel number!\n");
            return -1;
    }

    /* And then, we get the current active input type flag for this channel and display them 
     * on the OPI.
     * */
     /* since we are multi-threads now, so, we must clean the line, in case that the monitor thread send a query and 
      * we get the answer. That will be wrong answer for us.
      */
    read(pVar->com_fd, rcv_buff, 256 - 1);   
    if(ls240_queryIntype(inNum, channelInType, ssId, pVar) < 0)
    {
        printf("In list_curveFile(), Error query current channel input type!\n");
        return -1;        
    }

    snprintf(channelInType, sizeof(channelInType), "%s%c%c", receive_buff, '\n', '\0');  // prepare to call pickUpToken()
    cp = channelInType;
    printf("DEBUG:::1 In list_curve(): ---------channelInTyp=\'%s\'\n", channelInType);
    numTokens = pickOutToken(tokens, cp, delim);
    printf("DEBUG:::2 In list_curve(): ---------\n");
    if(numTokens != 6)
    {
        printf("In list_curve(): Intype query error!");
        return -1;
    }

    /* tokens[0] => sensor type; tokens[1] => auto range; tokens[2] => range; tokens[3] => current reversal
     * tokens[4] => sensor unit; tokens[5] => enable/disable.
     * Now, we first set auto-range, current-reversal, enable-disable bits into the channel-in-type-flag and
     * display them on the OPI. Second, we set the sensor type, range, sensor unit and display them on OPI. 
     * And, chXIntypeBits:  bit0 = enable/disable; bit1 = auto range; bit3 = current reversal
     */
    inFlagBit = strtod(tokens[1], &strp);
    if(inFlagBit != 0)
        inFlag |= inFlagBit <<  1; 
    
    inFlagBit = strtod(tokens[3], &strp);
    if(inFlagBit)
        inFlag |= inFlagBit << 2;
    
    inFlagBit = strtod(tokens[5], &strp);
    if(inFlagBit)
        inFlag |= inFlagBit;

    switch(inNum)
    {
        case 1:
            pVar->ch1IntypeBits = inFlag;
            seq_pvPut(ssId, pVar->ch1IntypeBitsIndex, SYNC);
            strncpy(pVar->ch1InType, receive_buff, 40);
            seq_pvPut(ssId, pVar->ch1InTypeIndex, SYNC);
            // solve: sensor type, sensor unit, input range herer   <<<<=========== TO DO !!!!!
            break;
        case 2:
            pVar->ch2IntypeBits = inFlag;
            seq_pvPut(ssId, pVar->ch2IntypeBitsIndex, SYNC);
            strncpy(pVar->ch2InType, receive_buff, 40);
            seq_pvPut(ssId, pVar->ch2InTypeIndex, SYNC);
            // ....
            break;
        case 3:
            pVar->ch3IntypeBits = inFlag;
            seq_pvPut(ssId, pVar->ch3IntypeBitsIndex, SYNC);
            strncpy(pVar->ch3InType, receive_buff, 40);
            seq_pvPut(ssId, pVar->ch3InTypeIndex, SYNC);
            // ....
            break;
        case 4:
            pVar->ch4IntypeBits = inFlag;
            seq_pvPut(ssId, pVar->ch4IntypeBitsIndex, SYNC);
            strncpy(pVar->ch4InType, receive_buff, 40);
            seq_pvPut(ssId, pVar->ch4InTypeIndex, SYNC);
            // ....
            break;
        case 5:
            pVar->ch5IntypeBits = inFlag;
            seq_pvPut(ssId, pVar->ch5IntypeBitsIndex, SYNC);
            strncpy(pVar->ch5InType, receive_buff, 40);
            seq_pvPut(ssId, pVar->ch5InTypeIndex, SYNC);
            // ....
            break;
        case 6:
            pVar->ch6IntypeBits = inFlag;
            seq_pvPut(ssId, pVar->ch6IntypeBitsIndex, SYNC);
            strncpy(pVar->ch6InType, receive_buff, 40);
            seq_pvPut(ssId, pVar->ch6InTypeIndex, SYNC);
            // ....
            break;
        case 7:
            pVar->ch7IntypeBits = inFlag;
            seq_pvPut(ssId, pVar->ch7IntypeBitsIndex, SYNC);
            strncpy(pVar->ch7InType, receive_buff, 40);
            seq_pvPut(ssId, pVar->ch7InTypeIndex, SYNC);
            // ....
            break;
        case 8:
            pVar->ch8IntypeBits = inFlag;
            seq_pvPut(ssId, pVar->ch8IntypeBitsIndex, SYNC);
            strncpy(pVar->ch8InType, receive_buff, 40);
            seq_pvPut(ssId, pVar->ch8InTypeIndex, SYNC);
            // ....
            break;
        default:
            printf("In list_curve(): Wrong channel number!\n");
            return -1;
    }

    // Now, we update the input-type in the main OPI



/**
 * @brief  find all curve files in the curve/ directory 
 * @note   
 * @retval 
 */
static int list_curveFiles(int inNum, SS_ID ssId, struct UserVar *pVar)
{
    DIR *dp = NULL;
    struct dirent *dptr = NULL;
    size_t nameLen;

    // Open the directory
    if(!(dp = opendir(curve_dir)))
    {
        printf("Error open directory: %s!\n", curve_dir);
        perror("Error open directory!");
        //exit(1);
        return -1;
    }

    // Read the contents of the dir and find out all the curve file and store them in an array to send to opi
    int i = 0;
    while((dptr = readdir(dp)))
    {
        // skip the . and ..
        nameLen = strlen(dptr->d_name);
        if(nameLen == 1 && dptr->d_name[0] == '.')
            continue;
        else if(nameLen == 2 && dptr->d_name[0] == '.' && dptr->d_name[1] == '.')
            continue;
        else if(dptr->d_type == DT_DIR || dptr->d_type == DT_LNK)  // skip a sub dir and soft link
            continue;   
        else if(strstr(dptr->d_name, ".340"))                      // a .340 curve file
        {
            dprintf("DEBUG::: the d_name = \'%s\'; length = %d\n", dptr->d_name, strlen(dptr->d_name));

            snprintf(pVar->curveFilesArray[i], 40, "%d%c%c%s", i+1, '.', ' ', dptr->d_name);   // EPICS string length = 40
            i++;
            if(i > MAXNUMCURVEFILE)
            {
                printf("Number of curve files > %d !\n", MAXNUMCURVEFILE);
                break;
            }
        }
        else if(dptr->d_type == DT_REG)                            // a test curve without surfix
        {
            dprintf("DEBUG::: the d_name = \'%s\'; length = %d\n", dptr->d_name, strlen(dptr->d_name));
            snprintf(pVar->curveFilesArray[i], 40, "%d%c%c%s", i+1, '.', ' ', dptr->d_name);   // EPICS string length = 40
            i++;
            if(i > MAXNUMCURVEFILE)
            {
                printf("Number of curve files > %d !\n", MAXNUMCURVEFILE);
                break;
            }
        }
        else{
            printf("Unkonw file type!!");
            continue;
        }
    }

    closedir(dp);

    return 0;
}

/**
 * @brief  Process the channel input type commands. 
 * @note   We have to keep track of the 8 channel Intype Flags, to detect any change caused by button click on the OPI which usually 
 *         issued a new command through the IOC to the device. 
 * @param  inNum: 
 * @param  ssId: 
 * @param  *pVar: 
 * @retval 
 */
static int process_3onOff(int inNum, SS_ID ssId, struct UserVar *pVar)
{
    //int inNum;
    if(do_3onoff_process(inNum, ssId, pVar) < 0)
    {
        printf("Erorr do_3onoff_process()!\n");
        return -1;
    }

    decide which channel input type(part of it, only the 3 on/off buttens) has been changed by operator, total 6, rest 3 need deal separatedly
    if(pVar->ch1IntypeBits)
    {
        inNum = 1;
        
        if(do_3onoff_process(inNum, ssId, pVar) < 0)
        {
            printf("Erorr do_3onoff_process()!\n");
            return -1;
        }

    }
    else if (pVar->ch2IntypeBits)
    {
        inNum = 2;

        if(do_3onoff_process(inNum, ssId, pVar) < 0)
        {
            printf("Erorr do_3onoff_process()!\n");
            return -1;
        }

    }
    else if (pVar->ch3IntypeBits)
    {
        inNum = 3;
        if(do_3onoff_process(inNum, ssId, pVar) < 0)
        {
            printf("Erorr do_3onoff_process()!\n");
            return -1;
        }    
    }
    else if (pVar->ch4IntypeBits)
    {    
        inNum = 4;
        if(do_3onoff_process(inNum, ssId, pVar) < 0)
        {
            printf("Erorr do_3onoff_process()!\n");
            return -1;
        }        
    }
    else if (pVar->ch5IntypeBits)
    {    
        inNum = 5;
        if(do_3onoff_process(inNum, ssId, pVar) < 0)
        {
            printf("Erorr do_3onoff_process()!\n");
            return -1;
        }        
    }
    else if (pVar->ch6IntypeBits)
    {
        inNum = 6;
        if(do_3onoff_process(inNum, ssId, pVar) < 0)
        {
            printf("Erorr do_3onoff_process()!\n");
            return -1;
        }        
    }
    else if (pVar->ch7IntypeBits)
    {
        inNum = 7;
        if(do_3onoff_process(inNum, ssId, pVar) < 0)
        {
            printf("Erorr do_3onoff_process()!\n");
            return -1;
        }        
    }
    else if (pVar->ch8IntypeBits)
    {
        inNum = 8;
        if(do_3onoff_process(inNum, ssId, pVar) < 0)
        {
            printf("Erorr do_3onoff_process()!\n");
            return -1;
        }        
    }

    return 0;
}


/**
 *  STOP PROFIBUS (!!!!! Seems not effect somehow )
 *
 */

static int ls240_setPROFISTOP(void)
{
   char setCMD[32];
   int rw_ret;

   snprintf(setCMD, 32, "%s%c%d%c", "PROFISTOP", ' ', 0, '\n');
   DBGPRINT("Command sent to device: %s\n", setCMD);
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }

   snprintf(setCMD, 32, "%c", '\n');
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }     

   wait_10ms();
   ls240_queryPROFISTAT();

   return 0;
}
