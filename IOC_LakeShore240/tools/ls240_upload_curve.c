/*
 * 
 *      Juntong Liu 
 *      @ European spallation source(ESS)
 *      email: juntongliu@ess.eu
 *                                                  2020-07-30
 * ------------------------------------------------------------
 * 
 *  This program can be used to upload curve to LakeShore240 temperature sensor input module through the USB port (according to LakeShore240 manual,
 *  once the PROFIBUS is active, one can not configure the LakeShore240 through the USB port any more. This probably means that the PROFIBUS must 
 *  be disconnected while uploading curve through the USB port. Need more check here!).
 *  
 *  Compile and use the program:
 * 
 *   1.) Open a terminal window and compile it with gcc:
 *
 *               gcc -Wall -o ls240_upload_curve  ls240_upload_curve.c
 * 
 *   2.) Run the program and following the prompt to upload curve:
 * 
 *               ./ls240_upload_curve
 * 
 *   3.) Upon running, the program will ask about curve file name, and which channel the curve should be uploaded to, ...etc. questions.
 * 
 *   4.) Curve file format:
 * 
 *         With the current version of the program, it only works with curve format similar to the format used by LakeShore240(.340 curve). The curve file 
 *         must have a header, something like the following:
 *         
 *         Sensor Model: LSCI_PT-100
 *         Serial number: Standard
 *         Data Format: 3 (Ohms vs. Kelvin)
 *         SetPoint limit: +800.000 (Kelvin)
 *         Temperature coefficient: 2 (Positive)
 *         Number of Breakpoints: 31
 *         Temperature Unit: (K)
 * 
 *        Following the header are the curve breakpoints(data pairs), like the following: 
 * 
 *         1   3.40500   28.500
 *         2   3.82000   30.500
 *             4.23500   32.500
 *             5.14600   36.500
 *         5   5.65000   38.500
 *         6   6.17000   40.500
 *         7   6.72600   42.500
 *         8   7.90900   46.500
 *         9   9.92400   52.500
 *        10   ................
 * 
 *       The breakpoint-line format is:      <Sequence Number(optional)>    <sensor unit>   <temperature>
 * 
 *       It is OK to have a sequence number before the breakpoints(data pair). And it is also OK without sequence number, or some lines have, some lines do not have.
 *       But, the most important thing is that, the sequence number(if have), sensor-unit and corresponding temperature must be separated with 
 *       whitespace(s), TAB(s) or both. Same rule apply to the header part of the curve file. 
 *       Blank lines at the begining of a curve file, or between the header and breakpoints will be ignored. In the curve header, key words: "Model:", "Number:", 
 *       "format:", "limit:",... must be preceed to its "values" and separated with whitespace(s), TAB(s) or both.
 *       Any lines begining with a "#" will be treated as comment lines.
 * 
 *   5.) Dump and save a channel's active curve into a file:
 * 
 *       This porgram offers an option to let user to choose to dump and save the current active curve into a file before replace it by uploading a new one.
 *
 *   6.) Turn on DEBUG, the program will print out more information.
 * 
 *   7.) This is a very primary version for LakeShore240 similar curve uploading test. The program has not been tested yet (may have bug).  
 *       New functions will be added to enable it to work with .340 format curve later.
 *                                                 
 *   8.) Note, for .340 curve file, the settings for the INTYPE in upload_curve() function need more check and may need change. 
 * 
 *                         
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
//#include <sys/stat.h>
//#include <linux/kernel.h>
#include <termios.h>
#include <stdarg.h>
#include <time.h>      // for nanosleep()
#include <stdbool.h>
#include <ctype.h>     // isdigit()

/* number of serial ports */
#define NUM_SPORTS 1
#define BUFFLEN 256    // max length can be read is 255 bytes
int com_fd;

fd_set select_fds;
char rcv_buff[BUFFLEN];

#define DEBUG
//#undef DEBUG
#ifdef DEBUG
#define dprintf(format, args...) printf(format, ##args)
#define DBGPRINT(fmt, ...)                            \
    do                                                \
    {                                                 \
        fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                __LINE__, __func__, __VA_ARGS__);     \
    } while (0)
#else
#define dprintf(format, args...)
#define DBGPRINT(fmt, ...)
#endif

#if 0
#ifdef DEBUG
#define DEBUG_print(fmt, ...)                             \
    do                                                    \
    {                                                     \
        if (DEBUG)                                        \
            fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                    __LINE__, __func__, __VA_ARGS__);     \
    } while (0)
#else
#define DEBUG_print(fmt, ...)
#endif
#endif

typedef struct ELLNODE
{
    struct ELLNODE *next;
    struct ELLNODE *previous;
} ELLNODE;

#define ELLNODE_INIT {NULL, NULL}

typedef struct ELLLIST
{
    ELLNODE node;
    int count;
} ELLLIST;

#define ELLLIST_INIT  {ELLNODE_INIT, 0}

typedef void (* FREEFUNC)(void *);

#define ellInit(PLIST){                                 \
    (PLIST)->node.next = (PLIST)->node.previous = NULL; \
    (PLIST)->count = 0;                                 \
}
#define ellCount(PLIST) ((PLIST)->count)
#define ellFirst(PLIST) ((PLIST)->node.next)
#define ellLast(PLIST) ((PLIST)->node.previous)
#define ellNext(PNODE) ((PNODE)->next)
#define ellPrevious(PNODE) ((PNODE)->previous)
#define ellFree(PLIST) ellFree2(PLIST, free)

/**
 * @brief  Add a node to the list
 * @note   
 * @param  *pList: 
 * @param  *pNode: 
 * @retval None
 */
void ellAdd(ELLLIST *pList, ELLNODE *pNode)
{
    pNode->next = NULL;
    pNode->previous = pList->node.previous;

    if (pList->count)
        pList->node.previous->next = pNode;
    else
        pList->node.next = pNode;

    pList->node.previous = pNode;
    pList->count++;

    return;
}

/**
 * @brief  Free the heap memory
 * @note   
 * @param  *pList: 
 * @param  freeFunc: 
 * @retval None
 */
void ellFree2(ELLLIST *pList, FREEFUNC freeFunc)
{
    ELLNODE *nnode = pList->node.next;
    ELLNODE *pnode;

    while (nnode != NULL)
    {
        pnode = nnode;
        nnode = nnode->next;
        freeFunc(pnode);
    }
    pList->node.next = NULL;
    pList->node.previous = NULL;
    pList->count = 0;
}

void freeNodes(void *pnode)
{
    free(pnode);
}

/**
 * @brief  Pick out tokens from a string separated by delimiters(, tabs, whitespace, .....), and skip the extra whitespaces and tabs
 * @note   
 * @param  **token:    a pointer array pointer to all the tokens
 * @param  *cp:       the string contain variable number whitespaces and TABs separated tokens
 * @param  *delimiter: string of chars as delimiter used by the function to pick out tokens
 * @retval number of tokens picked out
 */
static int pickOutToken(char **token, char *cp, const char *delimiter)
{
    int i = 0;

    while (*cp == ' ' || *cp == '\t')
        cp++;
    while ((token[i] = strsep(&cp, delimiter)))
    {
        while (*cp == ' ' || *cp == '\t')
            cp++;
        if (*cp == '\0' || *cp == '\n' || *cp == '\r')
            break;
        i++;
    };
    return i + 1;
}

struct curve_header {
    char sensorModel[15];                  // This actually is the curve name in the Lakeshore 240 manual, Max length = 15
    char serialNumber[10];                 // serial number,
    unsigned short dataFormat;             // data format: 2 = V/K, 3 = Ω/K, 4 = log Ω/K
    double setPointLimit;                  // set point limit. It is curve temperature limimt, value in kelvin
    unsigned short temperatureCoefficient; // coefficient: 1 = negative, 2 = positive
    unsigned short numberBreakpoints;      // number of break points of the curve, max 200 the Model-240 can take
    char tempUnit;                         // K = kelvin, C = celsius, F = fahrenheit 
    unsigned short curveFileFormat;        // 1 = .340 format curve, 2 = test-curve
};
char *serial_port = {"/dev/tty.SLAB_USBtoUART"};
struct curve_header curveHeader;
ELLLIST breakPointList;
typedef struct  breakPoint
{
    ELLNODE node;
    double sensor_unit;
    double temperature;
} breakPoint;

// enum {
//     MODEL=0, SERIAL, FORMAT, LIMIT, COEFFICIENT, BREAKPOINTS, UNIT
// }HEAD;
// Define header flag 
#define MODEL        0x01
#define NUMBER       0x02
#define FORMAT       0x04
#define LIMIT        0x08
#define COEFFICIENT  0x10
#define BREAKPOINTS  0x20
#define UNIT         0x40

/**
 * @brief   Called by main to initialize the virtual port - set the baud rate, ...etc.
 * @note   
 * @retval  0 = OK,   -1 = error 
 */
static int 
init_serial_ports(void)
{

    struct termios newOpt, oldOpt; // options;
    // Open the virtual port
    com_fd = open(serial_port, O_RDWR | O_NOCTTY | O_NONBLOCK);
    //com_fd = open(serial_port, O_RDWR | O_NOCTTY);
    if (com_fd < 0)
    {
        printf("#####################################\n");
        printf("Error opening USB virtual port!!!\n");
        printf("#####################################\n");
        return -1;
    };

    bzero(&newOpt, sizeof(struct termios));
    /* set terminal attributes.  */
    //  fcntl(com_fd[i], F_SETFL, 0);
    tcgetattr(com_fd, &oldOpt);
    cfsetispeed(&newOpt, 115200);
    cfsetospeed(&newOpt, 115200);

    /* xia mian xian tong, following is the first one work on com6 */
#if 0     
   newOpt.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP
			       | INLCR | IGNCR | ICRNL | IXON);
   newOpt.c_oflag &= ~OPOST;
   newOpt.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
   newOpt.c_cflag &= ~(CSIZE | PARENB);
   newOpt.c_cflag |= CS8;
#endif
    //newOpt.c_lflag |= (ICANON | ECHO | ECHOE);
    //newOpt.c_iflag |= (IGNPAR); // | IXON | IXOFF | IXANY);
    //newOpt.c_iflag |= (IXON | IXOFF | IXANY);     // enable software flow control
    //newOpt.c_oflag |= OPOST;
    newOpt.c_cflag |= (CS8 | CLOCAL | CREAD);
    newOpt.c_iflag &= ~(IXON | IXOFF | IXANY); // disable software flow control
    newOpt.c_oflag = 0;                        // oflag=0, use raw output
    newOpt.c_cc[VTIME] = 2;                    // set terminal i/o time out, 2 ok, 5 do not work
    newOpt.c_cc[VMIN] = 0;                     // blocking read util minimum 5 char received

// Set close on exec flag
#if defined(FD_CLOEXEC)
    if (fcntl(com_fd, F_SETFD, FD_CLOEXEC) < 0)
    {
        dprintf("Can't set close-on-exec flag!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
#endif

#if 0
   cfmakeraw(&newOpt);
   newOpt.c_cflag =  CLOCAL | CREAD;
   newOpt.c_iflag = IGNPAR;
   newOpt.c_oflag = 0;
   newOpt.c_lflag = 0;
   newOpt.c_cc[VMIN] = 1;
   newOpt.c_cc[VTIME] = 0;
#endif

    // Clean the line and active new setting
    tcflush(com_fd, TCIFLUSH);

    int tcret = tcsetattr(com_fd, TCSANOW, &newOpt);
    if (tcret < 0)
    {
        printf("tcsetattr() failed!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    //dprintf("com_fd = %d\n; ", com_fd);

    tcflush(com_fd, TCIOFLUSH);
    //tty->readTimeout = -1e-99;
    //tty->writeTimeout = -1e-99;
    if (fcntl(com_fd, F_SETFL, 0) < 0)
    {
        dprintf("Can't set F_SETFL file flags!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    //dprintf("\n Port fd=%d opened and options set\n", com_fd);

#if 0   
   /* get the old settings */
   tcgetattr(com_fds[0], &old_settings);

   /* prepare to set the attributes */
   new_settings.c_cflag = BAUD | CRTSCTS | DATABITS | STOPBITS | PARITYON | PARITY | CLOCAL | CREAD;
   new_settings.c_iflag = IGNPAR;
   new_settings.c_oflag = 0;
   new_settings.c_lflag = 0;          //ICANON;
   new_settings.c_cc[VMIN]=1;
   newsettings.c_cc[VTIME]=0;

   /* set ports' new attributes */
   for (i=1; i<3; i++)
   {
      tcflush(com_fds[i], TCIFLUSH);
      if(tcsetattr(fd, TCSANOW, &newtio) < 0)
      {
         sprintf(term_out, "%s\n", "################################");
         sprintf(term_out, "Serial port %d initialization failed!\n", i);
         sprintf(term_out, "%s\n", "################################");
         exit(-1);
      }
   }
#endif

    return 0;
}

/**
 * @brief  Delay 10 million seconds. Lakeshore240 requires a delay of 10 million seconds between commands.
 * @note   
 * @retval 
 */
static int wait_10ms(void)
{
    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = 1e+7; // 10ms  milliom second, 1000,000,000 nanoseconds = 1 second
    return nanosleep(&tim, &tim2);
}

/**
 * @brief  Called after a query command is sent to Lakeshore240 to get the device's reply
 * @note   
 * @retval 
 */
static int ls240_getReply(void)
{
    ssize_t rw_ret;
    int tries = 0, ret;
    char buff[8];
    ret = wait_10ms();
    DBGPRINT("nanosleep() returned with: %d\n", ret);
    while (1)
    {
        tries++;
        rw_ret = read(com_fd, rcv_buff, BUFFLEN - 1);             /* read the reply */
        if (strstr(rcv_buff, "\r\n") || strchr(rcv_buff, '\n'))
            break;
        if (rw_ret < 0)
        {
            perror("Read error from 240!");
            close(com_fd);
            com_fd = -1;
            return -1;
        }
    }
    // For a kind of flow control
    snprintf(buff, 8, "%c", '\n');
    rw_ret = write(com_fd, buff, strlen(buff));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        return -1;
    }
    DBGPRINT("Get the device reply after %d read() tries\n", tries);
    return 0;
}


/**
 * @brief  parse the curve file, build the curve header and pick out breakpoints to prepare
 *         to upload to the LakeShore240
 * @note   
 * @param fileName: curve file name
 * @retval 
 */
static int parse_curve(char *fileName)
{
    char *cp, *chp;
    char buff[512];
    int len;
    FILE *fp;
    char *tokens[8];
    int numToken;
    bool inBPsection = false;
    bool isBP = true;
    unsigned int headFlag = 0;
    char *delimiter = " \t\n\r";
    char *cp2;
    unsigned int numHeaderFields;

    fp = fopen(fileName, "r+");
    if (!fp)
    {
        perror("File open error!\n");
        return -1;
    }

    // we check the curve file format
    cp2 = strchr(fileName, '.');
    if(cp2 && !strncmp(cp2, ".340", strlen(cp2)))
    {
        numHeaderFields = 63;  // 5;
        curveHeader.curveFileFormat = 1;       // 1 = .340 format curve file
    }
    else {
        numHeaderFields = 127; // 6;
        curveHeader.curveFileFormat = 2;      // 2 = test-curve format
    }

    while (fgets(buff, sizeof(buff), fp))
    {    
        cp = buff;

        // Skip the possibl whitespaces and TABs before #
        while(*cp == '\t' || *cp == ' ')
            cp++;

        // If the first char is #, it is a comment line, and skip it
        if(*cp == '#')
            continue;
        
        // skip the empty line
        if(strlen(buff) == 1 && !feof(fp) && buff[0] == '\n')
            continue;

        // skip the breakpoints title: No.  Units\Temperature (K)
        if(strstr(cp, "No.") && strcasestr(cp, "Units\\Temperature"))
            continue;

        DBGPRINT("fgets() read in: %s\n strlen() return: %d, headFlag = %d\n", buff, (int)strlen(buff), headFlag);
        // Pick out the curve header
        //if(headFlag < 127)    // B1111111 = 127            <<------- the example curve file has 6 header fields
        //if(headFlag < 63)       // B11111 = 63               <<------- .340 format has 5 header fields
        if(headFlag < numHeaderFields)
        {
            if ((cp = strcasestr(buff, "Number:")))                           // Serial Number:
            {
                char *chp = strchr(cp, ':');
                if (chp == NULL)
                {
                    printf("Curve header format error!\n");
                    return -1;
                }
                numToken = pickOutToken(tokens, cp, delimiter);
                if(numToken == 2)
                {
                    len = strlen(tokens[1]);
                    if (len > 10)
                        strncpy(curveHeader.serialNumber, tokens[1], 10);     // serial <= 10 chars
                    else
                        strncpy(curveHeader.serialNumber, tokens[1], len);
                    dprintf("Serial number: %s\n", tokens[1]);
                }
                else 
                {
                    printf("Number field Error! curve header error!\n");
                    return -1;
                }
                headFlag |= MODEL;
            }
            else if ((cp = strcasestr(buff, "Model:")))                       // Sensor Model
            {
                char *chp = strchr(cp, ':');
                if (chp == NULL)
                {
                    printf("Curve header format error!\n");
                    return -1;
                }
                numToken = pickOutToken(tokens, cp, delimiter);
                if(numToken == 2)
                {
                    len = strlen(tokens[1]);
                    if (len > 15)
                        strncpy(curveHeader.sensorModel, tokens[1], 15);      // model name <= 15 chars
                    else
                        strncpy(curveHeader.sensorModel, tokens[1], len);
                    DBGPRINT("Sensor Model: %s\n", tokens[1]);
                }
                else
                {
                    printf("Model field Error! Curve header errro!\n");
                    return -1;
                }
                headFlag |= NUMBER;
            }
            else if ((cp = strcasestr(buff, "Limit:")))                       // Setpoint Limit
            {
                int setPointLimit;
                char *strp;
                char *chp = strchr(cp, ':');
                if (chp == NULL)
                {
                    printf("Curve header format error!\n");
                    return -1;
                }
                numToken = pickOutToken(tokens, cp, delimiter);
                if(numToken == 3)
                {
                    setPointLimit = strtod(tokens[1], &strp);
                    curveHeader.setPointLimit = setPointLimit;
                    dprintf("SetPoint Limit: %d\n", setPointLimit);
                }
                else
                {
                    printf("Limit field Error! Curve header error!");
                    return -1;
                }
                headFlag |= LIMIT;
            }
            else if ((cp = strcasestr(buff, "Coefficient:")))                // coefficient
            {
                int coeff;
                char *strp;
                chp = strchr(cp, ':');
                if (chp == NULL)
                {
                    printf("Curve header format error!\n");
                    return -1;
                }
                numToken = pickOutToken(tokens, cp, delimiter);
                if(numToken == 3)
                {
                    coeff = strtod(tokens[1], &strp);
                    curveHeader.temperatureCoefficient = coeff;
                    dprintf("coefficient: %d\n", coeff);
                }
                else
                {
                    printf("Coefficient field Error! Curve header error!");
                    return -1;
                }
                headFlag |= COEFFICIENT;
            }
            else if ((cp = strcasestr(buff, "Format:")))                // Data Format
            {
                int dataFmt;
                char *strp;
                chp = strchr(cp, ':');
                if (chp == NULL)
                {
                    printf("Curve header format error!\n");
                    return -1;
                }
                numToken = pickOutToken(tokens, cp, delimiter);
                // .340 has 4 tokens, example curve has 5 token
                //if(numToken == 5)
                if(numToken == 4 || numToken == 5)
                {
                    dataFmt = strtod(tokens[1], &strp);
                    curveHeader.dataFormat = dataFmt;
                    dprintf("Data format: %d\n", dataFmt);
                }
                else
                {
                    printf("Format field Error! Curve header error!");
                    return -1;
                }
                headFlag |= FORMAT;
            }
            else if ((cp = strcasestr(buff, "Unit:")))                  // Temperature unit     <<------------ .340 do not have this header field
            {
                char *chp = strchr(cp, ':');
                if (chp == NULL)
                {
                    printf("Curve header format error!\n");
                    return -1;
                }
                if (strcasestr(cp, "(K)"))
                    curveHeader.tempUnit = 'K';
                else if (strcasestr(cp, "(F)"))
                    curveHeader.tempUnit = 'F';
                else if (strcasestr(cp, "(C)"))
                    curveHeader.tempUnit = 'C';
                else if (strcasestr(cp, "(S)"))
                    curveHeader.tempUnit = 'S';
                else
                {
                    printf("Unknow temperature sensor unit!\n");
                    return -1;
                }
                dprintf("Temperature unit: %c\n", curveHeader.tempUnit);
                headFlag |= UNIT;
            }
            else if ((cp = strcasestr(buff, "Breakpoints:")))          // Number of breakpoints
            {
                int numBRKPT;
                char *strp;
                chp = strchr(cp, ':');
                if (chp == NULL)
                {
                    printf("Curve header format error!\n");
                    return -1;
                }
                numToken = pickOutToken(tokens, cp, delimiter);
                if(numToken == 2)
                {
                    numBRKPT = strtod(tokens[1], &strp);
                    curveHeader.numberBreakpoints = numBRKPT;
                    dprintf("Number of breakpoints: %d\n", numBRKPT);
                }
                else
                {
                    printf("Breakpoints field Error! Curve header error!");
                    return -1;
                }
                headFlag |= BREAKPOINTS;
            }
        }
        //else if(headFlag == 127)                                  // Pick out the breakpoints
        //else if(headFlag == 63)
        else if(headFlag == numHeaderFields)
        {   
            breakPoint *bpp;
            double sunit, temperat;
            char *strp;
            cp = buff; 

            // make sure, breakpoints are digital numbers
            while(*cp)
            {
                dprintf("%c", *cp);
                if(*cp != ' ' &&  *cp != '\t' && !isdigit(*cp) && *cp != '\n' && *cp != '\r' && *cp != '.')
                {   
                    isBP = false;
                    break;
                }
                else
                    cp++;
            }

            if(isBP == false && inBPsection == true)
            {
                printf("Parsing curve file: Breakpoint format error!!!!\n");
                return -1;
            }
            else if(isBP == false && inBPsection == false)
                continue;      // Skip lines we do not recognize, like extra header-lines; as long as it is not in the middle of breakpoints section, 

            cp = buff;
            dprintf("----------------------Read in breakpoint --------------- \n");
            DBGPRINT("Read in BreakPoint: %s\n", cp);
            numToken = pickOutToken(tokens, cp, delimiter);
            if(numToken == 3)
            {
                sunit = strtod(tokens[1], &strp);
                temperat = strtod(tokens[2], &strp);
            }
            else if(numToken == 2)
            {
                sunit = strtod(tokens[0], &strp);
                temperat = strtod(tokens[1], &strp);
            }
            else
            {
                printf("Error! Curve break point format error!");
                DBGPRINT("Number of tokens = %d, token0=%s, token2=%s\n", numToken, tokens[0], tokens[1]);
                return -1;
            }
            
            dprintf("BreakPoint sensor unit: \'%lf\'\n", sunit);
            dprintf("BreakPoint Temperature: \'%lf\'\n", temperat);
            cp = malloc(sizeof(struct breakPoint));
            if(!cp)
            {
                printf("Error!! Cannot allocate memory!\n");
                perror("Error allocate memory!");
                return -1;
            }
            memset(cp, 0, sizeof(struct breakPoint));
            bpp = (struct breakPoint *)cp;
            bpp->sensor_unit = sunit;
            bpp->temperature = temperat;
            ellAdd(&breakPointList, &bpp->node);
            inBPsection = true;
        }
        if(feof(fp))
            break;
    }

    if(feof(fp))
    {
        printf("\nFile parsing done!\n");
        DBGPRINT("Number of breakpoints of the curve: %d\n", ellCount(&breakPointList));
        fclose(fp);
        return 0;
    }
    else if(ferror(fp))
    {
        perror("Error reading file!<--------------\n");
        fclose(fp);
        return -1;
    }    

    return 0;
}

/**
 * @brief  After the curve file is parsed. The results are saved in a list and a structure to facilitate the following curve uploading procedure. 
 *         This function print out the parsed results from the in-memory list and a structure to see if it is OK.
 *         This function has been added an aditional check to make sure that the number of breakpoints should not exceed the maximum 200 and the 
 *         sensor units are in ascending order.
 * @note   
 * @retval  0 = OK, 
 *         -1 = Not OK, must stop curve uploading
 */
static int print_parse_result(void)
{
    ELLNODE *node;
    struct breakPoint *bpp;
    struct breakPoint *bpp2;

    dprintf("\n\n=============== Curve Header: ===============\n");
    dprintf("Sensor Model: %s\n", curveHeader.sensorModel);
    printf("Serial Number; %s\n", curveHeader.serialNumber);
    dprintf("Data Format: %d\n", curveHeader.dataFormat);
    dprintf("SetPoint Limit: %f\n", curveHeader.setPointLimit);
    dprintf("Coefficient: %d\n", curveHeader.temperatureCoefficient);
    dprintf("Temperature Unit: %c\n", curveHeader.tempUnit);
    dprintf("Number of breakpoints: %d\n", curveHeader.numberBreakpoints);
    dprintf("===============================================\n\n");

    node = ellFirst(&breakPointList);
    for (int i = 1; i < ellCount(&breakPointList)+1 ; i++)
    {
        bpp = (struct breakPoint *)node;
        bpp2 = (struct breakPoint *)node->next;
        printf("Breakpoint %i sensor unit: %f\n", i, bpp->sensor_unit);
        printf("Breakpoint %i temperature: %f\n\n", i, bpp->temperature);
        if(bpp2)
        {
            if(bpp2->sensor_unit < bpp->sensor_unit)   // make sure that the sensor units are in increasing order
            {
                printf("\n############ ERROR in curve!!! Sensor units must be in increasing order! ############\n\n");
                return -1;
            }
        }
        node = ellNext(node);
    }

    // Check to see if the number of breakpoints is right under 200 limit
    if(ellCount(&breakPointList) > 200)
    {
        printf("Error, the number of breakpoints can not bigger than 200!\n");
        return -1;
    }

    return 0;
}

/**
 * @brief  Get channel/sensor's input type. Reading:  <sensor type>,<autorange>,<current reversal>,<unit>,<on/off> 
 * @note   
 * @param inNum:     input number ( 1-8 ) 
 * @param inputType: query result 
 * @retval 
 */
static int ls240_queryIntype(int inNum, char *inputType)
{
    ssize_t rw_ret;
    char queryCMD[16];
    tcflush(com_fd, TCIOFLUSH);
    
    snprintf(queryCMD, 16, "%s%c%d%c", "INTYPE?", ' ', inNum, '\n');
    dprintf("The intype query cmd=%s\n", queryCMD);
    rw_ret = write(com_fd, queryCMD, strlen(queryCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
    // get device reply
    if (ls240_getReply() < 0)
    {
        printf("Reading device reply error!\n");
        return -1;
    }
    dprintf("Receive strlength=%lu, Sensor %d has input type: %s\n", strlen(rcv_buff), inNum, rcv_buff);
    DBGPRINT("Channel %d input type: %s\n", inNum, rcv_buff);
    strncpy(inputType, rcv_buff, strlen(rcv_buff));       // copy the queried input type to the caller
    //memset(rcv_buff, 0, BUFFLEN);
    
    return 0;
}

/**
 * @brief  Query Lakeshore 240 curve break points
 * @note   
 * @param  inNum:     The channel number 
 * @param  index:     The breakpoint index to query 
 * @retval 
 */
static int ls240_queryCRVPT(int inNum, int index)
{
    char queryCMD[16];
    int rw_ret;
    DBGPRINT("inNum = %d\n", inNum);
    if (inNum < 1 || inNum > 8)
    {
        printf("Error, invalid input number! It must be 1 - 8\n");
        return -1;
    }

    snprintf(queryCMD, 16, "%s%c%d%c%d%c", "CRVPT?", ' ', inNum, ',', index, '\n');
    DBGPRINT("Command to 240: %s\n", queryCMD);
    rw_ret = write(com_fd, queryCMD, strlen(queryCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    printf("write(): %d bytes data sent to 240\n", (int)rw_ret);

    // get device reply
    if (ls240_getReply() < 0)
    {
        printf("Error Reading device reply!\n");
        return -1;
    }    
    dprintf("Channel %d: breakpoint %d <Unit value>,<Temperature value> <<==>> \'%s\'\n", inNum, index, rcv_buff);
        
    //memset(rcv_buff, 0, BUFFLEN);
    
    return 0;
}

/**
 * @brief  Query curve header: <name>,<serial number>,<format>,<limit value>,<coefficient> 
 * @note   format:       2 = V/K, 3 = OHM/K, 4 = log ohm/K
 *         input = 0, query all 8 input header,
 *         coefficient:  1 = negative, 2 = positive
 * 
 * @param  input: 1,2,3,4,5,6,7,8;  query the specific input header
 * @retval 
 */
static int ls240_queryCRVHDR(char *crvHeader, int input)
{
    char queryCMD[16];
    int rw_ret;

    if (input < 0 || input > 8)
    {
        printf("Error, input must be one of 0,1,2,3,4,5,6,7,8\n");
        return -1;
    }

    snprintf(queryCMD, 12, "%s%c%d%c", "CRVHDR?", ' ', input, '\n');

    dprintf("Query input %d curve header cmd = %s\n", input, queryCMD);
    rw_ret = write(com_fd, queryCMD, strlen(queryCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

    // get device reply
    if (ls240_getReply() < 0)
    {
        printf("Error Reading device reply!\n");
        return -1;
    }
    dprintf("Channel %d curve header is: \'%s\'\n", input, rcv_buff);
    strncpy(crvHeader, rcv_buff, strlen(rcv_buff));
    
    printf("\n<name>,<serial number>,<format>,<limit value>,<coefficient>\n");
    printf("Format: 2 = V/K, 3 = ohm/K, 4 = log ohm/K. coefficient: 1 = negative, 2 = positive\n");
    memset(rcv_buff, 0, BUFFLEN);

    return 0;
}

/**
 * @brief  Dump and save the channel's current curve into file before replace it.
 * @note   
 * @param  inNum: Channel number
 * @retval 
 */
static int ls240_dumpSaveCurve(char *fileName, int inNum)
{
    FILE *fp;
    time_t theTime;
    char *cp, *str;
    char *tokens[8];
    char crvHeader[64];
    char tmp[36];
    int hdNum;
    char *timeStr;
    double sensorUnit, temperature;
    char *delimiter1 = " \t\n\r";
    char *delimiter2 = ",\n\r";
    char *delimiter3 = " ,\t\n\r";
    // make an unique file name
    time(&theTime);
    timeStr = ctime(&theTime);
    
    pickOutToken(tokens, timeStr, delimiter1);
    
    dprintf("DEBUG:::-----token3 is time: \'%s\'\n", tokens[3]);
    
    cp = strchr(tokens[3], ':');   // replace the 2 ':'  with '_' in the time string, the ':' needs escape to display on some systems
    *cp = '_';
    cp = strchr(tokens[3], ':');
    *cp = '_';
    snprintf(fileName, 128, "%s%d%s%c%s%s%s%c%s", "Channel", inNum, "Curve", '-', tokens[4], tokens[1], tokens[2], '-', tokens[3]);
    dprintf("File name: %s\n", fileName);
    fp = fopen(fileName, "wa+");
    if (!fp)
    {
        perror("File open error!\n");
        return -1;
    }

    memset(rcv_buff, 0, sizeof(rcv_buff));
    if (ls240_queryCRVHDR(crvHeader, inNum) < 0)
    {
        printf("Error query curve header!\n");
        //return -1;
    }
    cp = crvHeader;
    pickOutToken(tokens, cp, delimiter2);
    snprintf(tmp, 36, "%s%c%s%c", "Sensor Model:", ' ', tokens[0], '\n');
    if (fputs(tmp, fp) == EOF)
    {
        perror("File write error!");
        fclose(fp);
        return -1;
    }
    snprintf(tmp, 36, "%s%c%s%c", "Serial Number:", ' ', tokens[1], '\n');
    if (fputs(tmp, fp) == EOF)
    {
        perror("File write error!");
        fclose(fp);
        return -1;
    }
    hdNum = atoi(tokens[2]);
    snprintf(tmp, 36, "%s%c%d%c", "Data Formt:", ' ', hdNum, '\n');
    if (fputs(tmp, fp) == EOF)
    {
        perror("File write error!");
        fclose(fp);
        return -1;
    }
    snprintf(tmp, 36, "%s%c%s%c", "Setpoint Limit:", ' ', tokens[3], '\n');
    if (fputs(tmp, fp) == EOF)
    {
        perror("File write error!");
        fclose(fp);
        return -1;
    }

    hdNum = atoi(tokens[4]);
    snprintf(tmp, 36, "%s%c%d%c", "Temperature Coefficient:", ' ', hdNum, '\n');
    if (fputs(tmp, fp) == EOF)
    {
        perror("File write error!");
        fclose(fp);
        return -1;
    }

    snprintf(tmp, 36, "%c", '\n');
    if (fputs(tmp, fp) == EOF)
    {
        perror("File write error!");
        fclose(fp);
        return -1;
    }   

    for (int i = 1; i < 200; i++)
    {
        ls240_queryCRVPT(inNum, i);
        if(!(cp = strchr(rcv_buff, ',')))
        {
            perror("Breakpoint format error!");
            return -1;
        }
        *cp = '\t';
        if(fputs(rcv_buff, fp) == EOF)
        {
            perror("File write error!");
            fclose(fp);
            return -1;
        }

        cp = rcv_buff;
        pickOutToken(tokens, cp, delimiter3);
        sensorUnit = strtod(tokens[0], &str);
        DBGPRINT("%d sensor unit: %f\n", i, sensorUnit);
        temperature = strtod(tokens[1], &str);
        DBGPRINT("%d temperature: %f\n\n", i, temperature);
        if(sensorUnit == 0 && temperature == 0)
            break;
    }
    fclose(fp);

    return 0;
}

/**
 * @brief  Upload a channel curve to LakeShore240
 * @note   
 * @param  channel: The channel which will receive the uploaded curve
 * @retval 
 */
static int upload_curve(int inNum)
{
    char crvCMD[64];
    char setCMD[128];
    int rw_ret;
    ELLNODE *node;
    struct breakPoint *bpp;
    double sensorUnit, temperature;
    double temp;
    char inputType[16];
    int fret;
    char *token, *str, instr[8];
    int len, len1, len2;
    char *cp;

    // Query and save the current input type of the channel
    if (ls240_queryIntype(inNum, inputType) < 0)
    {
        printf("Error query input type!\n");
        return -1;
    }

    // Query the channel curve's first breakpoint 
    fret = ls240_queryCRVPT(inNum, 1);
    cp = rcv_buff;
    if ((token = strsep(&cp, ",")))
    {
        sensorUnit = strtod(token, &str);
        len1 = strlen(token);
    }
    else
    {
        printf("Curve format error\n");
        return -1;
    }
    len = strlen(token);
    //token += (len + 1);
    temperature = strtod(cp, &str);
    len2 = strlen(cp);

    printf("Channel %d curve will be overwritten/replaced, continue?(Y/N): ", inNum);
    if (!fgets(instr, 8, stdin))
    {
        perror("fgets error:");
        exit(0);
    }
    if (instr[0] != 'Y' && instr[0] != 'y' && instr[0] != 'N' && instr[0] != 'n')
    {
        printf("Wrong answer! The answer must be \"Y\", \"y\", \"N\" or \"n\" ! \n");
        return -1;
    }
        
    if (instr[0] == 'N' || instr[0] == 'n')
    {
        printf("Curve uploading is quited!\n");
        return -1;
    }

    bzero(crvCMD, sizeof(crvCMD));
    snprintf(crvCMD, 64, "%s%c%d%c", "CRVDEL", ' ', inNum, '\n');
    dprintf("Command to 240: %s\n", crvCMD);
    rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
    wait_10ms();

    // Set a new curve header. If the curve file is a .340 file, we use real Serial Number, otherwise, use NONE for a customer curve file. 
    if(curveHeader.curveFileFormat == 1)
        snprintf(setCMD, 128, "%s%c%d%c%c%s%c%c%c%s%c%c%d%c%f%c%d%c", "CRVHDR", ' ', inNum, ',', '"', curveHeader.sensorModel, '"', ',', '"', curveHeader.serialNumber, '"',
             ',', curveHeader.dataFormat, ',', curveHeader.setPointLimit, ',', curveHeader.temperatureCoefficient, '\n');
    else if(curveHeader.curveFileFormat == 2)
        snprintf(setCMD, 128, "%s%c%d%c%c%s%c%c%c%s%c%c%d%c%f%c%d%c", "CRVHDR", ' ', inNum, ',', '"', curveHeader.sensorModel, '"', ',', '"', "NONE", '"',
             ',', curveHeader.dataFormat, ',', curveHeader.setPointLimit, ',', curveHeader.temperatureCoefficient, '\n');
    else {
        printf("Unkonw curve file format!\n");
        return -1;
    }
    dprintf("Command to 240: %s\n", setCMD);
    rw_ret = write(com_fd, setCMD, strlen(setCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
    wait_10ms();

    // Set input type
    bzero(crvCMD, sizeof(crvCMD));
    snprintf(crvCMD, 64, "%s%c%d%c%s%c", "INTYPE", ' ', inNum, ',', "2,0,0,0,1,1", '\n');        // NEED CHECK, MAY NEED CHANGE LATER!!!!!!
    dprintf("Command to 240: %s\n", crvCMD);
    rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
    wait_10ms();

    node = ellFirst(&breakPointList);
    DBGPRINT("Number of breakpoints on the list is: %d\n", ellCount(&breakPointList)+1 );

    for (int i = 1; i < ellCount(&breakPointList)+1; i++)
    {
        bpp = (struct breakPoint *)node;
        sensorUnit = bpp->sensor_unit;
        temp = bpp->temperature;
        bzero(crvCMD, sizeof(crvCMD));
        snprintf(crvCMD, 64, "%s%c%d%c%d%c%f%c%f%c", "CRVPT", ' ', inNum, ',', i, ',', sensorUnit, ',', temp, '\n');
        DBGPRINT("Command to 240 to set breakpoint: %s\n", crvCMD);
        rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
        if (rw_ret < 0)
        {
            perror("Error write to 240!\n");
            close(com_fd);
            com_fd = -1;
            return -1;
        }
        printf("write(): %d bytes data sent to 240\n", (int)rw_ret);
        dprintf("\'%s\'\n", crvCMD);
        bzero(crvCMD, sizeof(crvCMD));
        wait_10ms();
        node = ellNext(node);
    }
    wait_10ms();

    // query the new header
    bzero(crvCMD, sizeof(crvCMD));
    snprintf(crvCMD, 64, "%s%c%d%c", "CRVHDR?", ' ', inNum, '\n');
    dprintf("Query new header command to 240: %s\n", crvCMD);
    rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
    wait_10ms();

    // query the new input type
    bzero(crvCMD, sizeof(crvCMD));
    snprintf(crvCMD, 64, "%s%c%d%c", "INTYPE?", ' ', inNum, '\n');
    dprintf("Query new input type command to 240: %s\n", crvCMD);
    rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
    wait_10ms();

    bzero(crvCMD, sizeof(crvCMD));
    snprintf(crvCMD, 64, "%s%c%d%c%d%c", "FILTER", ' ', inNum, ' ', 100, '\n');     // Model 8P always 100
    dprintf("Set filter to the input: %s\n", crvCMD);
    rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
    wait_10ms();

    bzero(crvCMD, sizeof(crvCMD));
    snprintf(crvCMD, 64, "%s%c%d%c", "FILTER?", ' ', inNum, '\n');
    dprintf("Set filter to the input: %s\n", crvCMD);
    rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }
    dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
    wait_10ms();

    bzero(crvCMD, sizeof(crvCMD));
    snprintf(crvCMD, 64, "%c", '\n');
    rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        com_fd = -1;
        return -1;
    }

    return 0;
}


/**
 * @brief  Main function
 * @note   
 * @retval 
 */
int main()
{
    char instr[8];
    char newCrvFileName[128];
    char dumpedCrvFileName[128];
    int inNum; 
    int len;
    char saveCurrentCurve[8];
    
    if (init_serial_ports() < 0)
    {
        printf("Serial port initialization error!\n");
        exit(-1);
    }

    printf("\n\n###################################################################\n\n");
    printf("To upload a curve to Lakeshore 240, one must decide the following:\n\n");
    printf("1.) Which curve file to upload.\n");
    printf("2.) Which input/channel the curve should be uploaded to(1-8).\n\n");
    printf("###################################################################\n\n");

    printf("\nWhich curve to upload(give curve file name. Default location is current working directory)?: ");
    if (!fgets(newCrvFileName, 128, stdin))
    {
        perror("fgets error:");
        exit(0);
    }
    len = strlen(newCrvFileName);
    newCrvFileName[len-1] = '\0';
    DBGPRINT("File to upload: \'%s\' \n", newCrvFileName);
    //stat(fileName, &curveStat);
    if(access(newCrvFileName, R_OK) != 0)
    {
        printf("Error! File does not exist!\n");
        perror("Error to access file\n");
        return -1;
    }

    printf("Which channel/input, the curve should be uploaded to(1-8)? ");
    if (!fgets(instr, 8, stdin))
    {
        perror("fgets error:");
        exit(0);
    }
    inNum = atoi(instr);
    DBGPRINT("inNum = %d\n", inNum);
    if (inNum < 1 || inNum > 8)
    {
        printf("Error, invalid input number! It must be 1 - 8\n");
        return -1;
    }

    ellInit(&breakPointList);
    if(parse_curve(newCrvFileName) < 0)
    {
        printf("Error parse curve file!\n");
        return -1;
    }

    if(print_parse_result() < 0)
    {
        printf("Invalid number of breakpoints! Curve uploading quited!\n");
        return -1;
    }

    printf("Do you want to save the active curve of channel %d into a file before overwrite it?(Y/N): ", inNum);
    if (!fgets(saveCurrentCurve, 8, stdin))
    {
        perror("fgets error:");
        exit(0);
    }
    if (saveCurrentCurve[0] != 'Y' && saveCurrentCurve[0] != 'y' && saveCurrentCurve[0] != 'N' && saveCurrentCurve[0] != 'n')
    {
        printf("Wrong answer! The answer must be \"Y\", \"y\", \"N\" or \"n\" ! \n");
        return -1;
    }

    if (saveCurrentCurve[0] != 'N' && saveCurrentCurve[0] != 'n')
    {
        if(ls240_dumpSaveCurve(dumpedCrvFileName, inNum) < 0)
        {
            printf("Error dumping channel curve!\n");
            return -1;
        }
    }

    if (upload_curve(inNum) < 0)
    {
        printf("Error upload curve file!\n");
        return -1;
    }

    ellFree2(&breakPointList, freeNodes);

    printf("\n\n#######################################################################################################\n\n");
    printf("Curve for channel %d has been succeffully uploaded!\n", inNum);
    printf("The lakeshore240_usb_com program can be used to check to see if the curve has been uploaded correctly.\n");
    if (saveCurrentCurve[0] != 'N' || saveCurrentCurve[0] != 'n')
        printf("Previous curve of channel %d has been saved in file: %s\n\n", inNum, dumpedCrvFileName);
    printf("#######################################################################################################\n\n\n");

    return 0;
}

#if 0
static int test_func(void)
{

}
#endif
