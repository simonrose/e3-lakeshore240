/* 
 *
 *   Juntong Liu
 *   @ European Spallation Source(ESS)
 *   email: juntong.liu@ess.eu
 *                                          2020.07.12
 * 
 * 
 *   File name: lakeshore240_usb_com.c 
 *  
 *   Purpose:  
 *             1.) Test Lakeshore240 USB-UART virtual port communication.
 * 
 *             2.) Test Lakeshore240 commands-set, like read temperature measurements, read device ID from Lakeshore240, set address ....etc.
 *
 * 
 *   Compile and run it:
 * 
 *             1.) open a terminal window and compile it with gcc:
 *                
 *                       gcc -Wall -o lakeshore_usb_com  lakeshore240_usb_com.c
 *             
 *              2.) Run the tool with -h to see all the commands: 
 *        
 *                      ./lakeshore240_usb_com -h
 *             
 *              3.) Run a specific command:
 *            
 *                      ./lakeshore240_usb_com   <LakeShore240-Command>
 * 
 * 
 *   Usage:  
 *           Run this tool without commandline argument, the tool will execute some basic Lakeshore240 commands to 
 *           query the Lakeshore240 and print out the replies. 
 *           This tool also can be run with a Lakeshore240 command as a commandline argument, in this case, only the 
 *           Lakeshore240 command passed to the tool will be excuted. This is mainly for facilitating the test of 
 *           individual Lakeshore240 command.  
 * 
 *           Turn off DEBUG if one wants less information to be printed out.    
 * 
 *           Note, this program has not been tested yet (may have bug).                                                      
 *           
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
//#include <linux/kernel.h>
#include <termios.h>
#include <stdarg.h>
#include <time.h>  // for nanosleep()

/* number of serial ports */
#define NUM_SPORTS 1  
#define BUFFLEN   256       // Lakeshore240: max chars that can be read is 255 bytes
int com_fd;        

fd_set select_fds;
char rcv_buff[BUFFLEN];

#define DEBUG
//#undef DEBUG
#ifdef DEBUG
#define dprintf(format, args...) printf(format, ##args)
#define DBGPRINT(fmt, ...)                             \
   do                                                  \
   {                                                   \
         fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                 __LINE__, __func__, __VA_ARGS__);     \
   } while (0)
#else
#define dprintf(format, args...)
#define DBGPRINT(fmt, ...)
#endif

#if 0
#ifdef DEBUG
#define DEBUG_print(fmt, ...)                          \
   do                                                  \
   {                                                   \
      if (DEBUG)                                       \
         fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                 __LINE__, __func__, __VA_ARGS__);     \
   } while (0)
#else
#define DEBUG_print(fmt, ...)
#endif 
#endif 

/* when debugg is on, we dump serial output message to terminal */ 
//FILE *term_out;
enum 
{
   Q_IDN=1, Q_ADDR, ADDR,
   Q_BRIGT, BRIGT, Q_CRDG,
   CRVDEL, Q_CRVHDR, CRVHDR,
   Q_CRVPT, CRVPT, DFLT, 
   Q_FILTER, FILTER, Q_FRDG,
   Q_INNAME, INNAME, Q_INTYPE,
   INTYPE, Q_KRDG, Q_MODNAME,
   MODNAME, Q_PROFINUM, PROFINUM,
   Q_PROFISLOT, PROFISLOT, Q_PROFISTAT,
   Q_RDGST, Q_SRDG, UPLOADCRV, PROFISTOP
} LSCMD;
char *ls240CMD = "Q_IDN Q_INTYPE Q_MODNAME Q_KRDG MODNAME DFLT Q_INNAME Q_ADDR Q_BRIGT BRIGT Q_CRVHDR Q_CRVPT ADDR Q_CRDG CRVDEL CRVHDR CRVPT \
     Q_FILTER FILTER Q_FRDG  INNAME  INTYPE Q_PROFINUM \
     PROFINUM Q_PROFISLOT  PROFISLOT  Q_PROFISTAT Q_RDGST Q_SRDG UPLOADCRV PROFISTOP"; 
char *serial_port = {"/dev/tty.SLAB_USBtoUART"};
char *ls240CMD_DONE = "Q_IDN Q_INTYPE Q_MODNAME Q_KRDG MODNAME DFLT Q_INNAME Q_ADDR Q_BRIGT BRIGT Q_CRVHDR Q_CRVPT INNAME UPLOADCRV Q_PROFISTAT \
      Q_PROFISLOT Q_PROFINUM PROFINUM PROFISLOT ADDR PROFISTOP"; /* Q_CRDG CRVDEL CRVHDR CRVPT \
      Q_FILTER FILTER Q_FRDG  INTYPE  \
      Q_RDGST Q_SRDG"; */

/* Function prototype */
static int wait_10ms(void);
static int init_serial_ports(void);
static int ls240_getReply(void);
static int ls240_queryDeviceID(void);
static int ls240_queryIntype(void);
static int ls240_readTemperatures(void);
static int ls240_queryModuleName(void);
static int ls240_setDFLT(void);
static int ls240_setModuleName();
static int ls240_queryINNAME(int);
static int ls240_queryADDR(void);
static int ls240_queryBRIGT(void);
static int ls240_setBRIGT(void);
static int ls240_queryCRVHDR(int);
static int ls240_queryCRVPT(void);
static int ls240_upload_curve(void);
static int ls240_setINNAME(void);
static int ls240_queryPROFISTAT(void);
static int ls240_queryPROFISLOT(int input);
static int ls240_setPROFINUM(void);
static int ls240_setPROFISLOT(void);
static int ls240_setAddr(void);
static int ls240_setPROFISTOP(void);

/**
 * @brief  Pick out tokens from a string
 * @note   
 * @param  **token: 
 * @param  *cp: 
 * @param  *delimiter: 
 * @retval 
 */
static int pickOutToken(char **token, char *cp, const char *delimiter)
{
    int i = 0;

    while (*cp == ' ' || *cp == '\t')
        cp++;
    while ((token[i] = strsep(&cp, delimiter)))
    {
        while (*cp == ' ' || *cp == '\t')
            cp++;
        if (*cp == '\0' || *cp == '\n' || *cp == '\r')
            break;
        i++;
    };
    return i + 1;
}

/**
 * @brief  Called by main to initialize the virtual port - set the baud rate, ...etc.
 * @note   
 * @retval 0 -- OK,   -1 -- error 
 */
static int init_serial_ports(void)
{

   struct termios newOpt, oldOpt; // options;
   // Open the virtual port
   com_fd = open(serial_port, O_RDWR | O_NOCTTY | O_NONBLOCK);
   //com_fd = open(serial_port, O_RDWR | O_NOCTTY);
   if (com_fd < 0)
   {
      printf("################################\n");
      printf("Error opening USB virtual port!!!\n");
      printf("################################\n");
      return -1;
   };

   bzero(&newOpt, sizeof(struct termios));
   /* set terminal attributes.  */
   //  fcntl(com_fd[i], F_SETFL, 0);
   tcgetattr(com_fd, &oldOpt);
   cfsetispeed(&newOpt, 115200);
   cfsetospeed(&newOpt, 115200);

   /* xia mian xian tong, following is the first one work on com6 */
#if 0     
   newOpt.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP
			       | INLCR | IGNCR | ICRNL | IXON);
   newOpt.c_oflag &= ~OPOST;
   newOpt.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
   newOpt.c_cflag &= ~(CSIZE | PARENB);
   newOpt.c_cflag |= CS8; 
#endif
   //newOpt.c_lflag |= (ICANON | ECHO | ECHOE);
   //newOpt.c_iflag |= (IGNPAR); // | IXON | IXOFF | IXANY);
   //newOpt.c_iflag |= (IXON | IXOFF | IXANY);     // enable software flow control
   //newOpt.c_oflag |= OPOST;
   newOpt.c_cflag |= (CS8 | CLOCAL | CREAD);
   newOpt.c_iflag &= ~(IXON | IXOFF | IXANY);      // disable software flow control
   newOpt.c_oflag = 0;                             // oflag=0, use raw output
   newOpt.c_cc[VTIME] = 2;                         // set terminal i/o time out, 2 ok, 5 do not work
   newOpt.c_cc[VMIN] = 0;                          // blocking read util minimum 5 char received

// Set close on exec flag
#if defined(FD_CLOEXEC)
   if (fcntl(com_fd, F_SETFD, FD_CLOEXEC) < 0)
   {
      dprintf( "Can't set close-on-exec flag!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
#endif 

#if 0
   cfmakeraw(&newOpt);
   newOpt.c_cflag =  CLOCAL | CREAD;
   newOpt.c_iflag = IGNPAR;
   newOpt.c_oflag = 0;
   newOpt.c_lflag = 0;
   newOpt.c_cc[VMIN] = 1;
   newOpt.c_cc[VTIME] = 0;
#endif
      
   // Clean the line and active new setting
   tcflush(com_fd, TCIFLUSH);

   int tcret = tcsetattr(com_fd, TCSANOW, &newOpt); 
   if(tcret < 0)
   {
      printf("tcsetattr() failed!\n");
	   close(com_fd);
      com_fd = -1;
      return -1;
   }
   //dprintf("com_fd = %d\n; ", com_fd);

   tcflush(com_fd, TCIOFLUSH);
   //tty->readTimeout = -1e-99;
   //tty->writeTimeout = -1e-99;
   if (fcntl(com_fd, F_SETFL, 0) < 0)
   {
      dprintf("Can't set F_SETFL file flags!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   //dprintf("\n Port fd=%d opened and options set\n", com_fd);

#if 0   
   /* get the old settings */
   tcgetattr(com_fds[0], &old_settings);

   /* prepare to set the attributes */
   new_settings.c_cflag = BAUD | CRTSCTS | DATABITS | STOPBITS | PARITYON | PARITY | CLOCAL | CREAD;
   new_settings.c_iflag = IGNPAR;
   new_settings.c_oflag = 0;
   new_settings.c_lflag = 0;          //ICANON;
   new_settings.c_cc[VMIN]=1;
   newsettings.c_cc[VTIME]=0;

   /* set ports' new attributes */
   for (i=1; i<3; i++)
   {
      tcflush(com_fds[i], TCIFLUSH);
      if(tcsetattr(fd, TCSANOW, &newtio) < 0)
      {
         sprintf(term_out, "%s\n", "################################");
         sprintf(term_out, "Serial port %d initialization failed!\n", i);
         sprintf(term_out, "%s\n", "################################");
         exit(-1);
      }
   }
#endif 
     
   return 0; 
}

/**
 * @brief  Delay 10 million second. Lakeshore240 requires a delay of 10 million seconds between commands. 
 * @note   
 * @retval 0 = delay 10ms; positive value mean under-delayed; negtive value mean over-delayed
 */
static int wait_10ms(void)
{
   //int ret;
   struct timespec tim, tim2;
   tim.tv_sec = 0;
   tim.tv_nsec = 1e+7;           // 10ms  milliom second, 1000,000,000 nanoseconds = 1 second
   return nanosleep(&tim, &tim2);
 
}


/**
 * @brief  Called after a query command is sent to Lakeshore240 to get the device's reply
 * @note   
 * @retval 
 */
static int ls240_getReply(void)
{
   ssize_t rw_ret;
   char buff[8];
   int tries = 0, ret;
   ret = wait_10ms();
   DBGPRINT("nanosleep() returned with: %d\n", ret);
   while (1)
   {
      //pos++;
      tries++;
      rw_ret = read(com_fd, rcv_buff, BUFFLEN - 1);             /* read the reply */
      if (strstr(rcv_buff, "\r\n") || strchr(rcv_buff, '\n'))
         break;
      if (rw_ret < 0)
      {
         perror("Read error from 240!");
         close(com_fd);
         com_fd = -1;
         return -1;
      }
   }

   // For a kind of flow control
   snprintf(buff, 8, "%c", '\n');
    rw_ret = write(com_fd, buff, strlen(buff));
    if (rw_ret < 0)
    {
        perror("Error write to 240!\n");
        close(com_fd);
        return -1;
    }
   DBGPRINT("Get the device reply after %d read() tries\n", tries);
   return 0;
}


/**
 * @brief  Get device ID (manufacture,model,serial,firm version)
 * @note   
 * @retval 
 */
static int ls240_queryDeviceID(void)
{
   ssize_t rw_ret;
   tcflush(com_fd, TCIOFLUSH);
   rw_ret = write(com_fd, "*IDN?\n", 6);
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
   dprintf("Device ID query, cmd=%s\n", "*IDN?");
   // get device reply
   if(ls240_getReply() < 0)
   {
      printf("Error reading device reply\n");
   }
   else
      printf("\nDevice ID is: %s\n", rcv_buff);
   memset(rcv_buff, 0, BUFFLEN);

   return 0;
}


/**
 * @brief  Get channel/sensor's intype. Reading:  <sensor type>,<autorange>,<current reversal>,<unit> 
 * @note   
 * @retval 
 */
static int ls240_queryIntype(void)
{
   ssize_t rw_ret;
   char intypeQuery[16];
   tcflush(com_fd, TCIOFLUSH);
   for (int i = 1; i < 9; i++)
   {
      //int j = i + 1;
      snprintf(intypeQuery, 11, "%s%c%d%c", "INTYPE?", ' ', i, '\n');
      dprintf("The intype query cmd=%s\n", intypeQuery);
      rw_ret = write(com_fd, intypeQuery, strlen(intypeQuery));
      if (rw_ret < 0)
      {
         perror("Error write to 240!\n");
         close(com_fd);
         return -1;
      }
      dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
      // get device reply
      if(ls240_getReply() < 0)
         printf("Reading device reply error!\n");
      else
         printf("Receive strlength=%lu, Sensor %d has input type: %s\n", strlen(rcv_buff), i, rcv_buff);
         //dprintf("240 reply after %d tries, length = %d, contents = %s\n", tries, (int)rw_ret, rcv_buff);
      memset(rcv_buff, 0, BUFFLEN);
   }
   return 0;
}


/**
 * @brief  Read temperature from Lakeshore 240 in Kelvin unit
 * @note   
 * @retval 
 */
static int ls240_readTemperatures(void)
{
   ssize_t rw_ret;
   char krdgQuery[16];
   tcflush(com_fd, TCIOFLUSH);
   for (int i = 1; i < 5; i++)
   {
      //int j = i + 1;
      snprintf(krdgQuery, 9, "%s%c%d%c", "KRDG?", ' ', i, '\n');
      dprintf("Read temperature query cmd=%s\n", krdgQuery);
      rw_ret = write(com_fd, krdgQuery, strlen(krdgQuery));
      if (rw_ret < 0)
      {
         perror("Error write to 240!\n");
         close(com_fd);
         return -1;
      }
      dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

      // get device reply
      if(ls240_getReply()  < 0)
         printf("Error Reading device reply!\n");
      else   
         printf("Sensor %d has temperature: %s(K)\n", i, rcv_buff);
      memset(rcv_buff, 0, BUFFLEN);
   }
   return 0;
}


/**
 * @brief  Read device model name. The Lakeshore 240 we have should have a model name: Model240-8P
 * @note   
 * @retval 
 */
static int ls240_queryModuleName(void)
{
   ssize_t rw_ret;
   char moduleName[16];
   tcflush(com_fd, TCIOFLUSH);
   snprintf(moduleName, 10, "%s%c", "MODNAME?", '\n');

   rw_ret = write(com_fd, moduleName, strlen(moduleName));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
   dprintf("Device module name query, cmd=%s\n", moduleName);
   // get device reply
   if (ls240_getReply() < 0)
      printf("Error Reading device reply!\n");
   else
      printf("\nDevice module name is: %s\n", rcv_buff);
   bzero(rcv_buff, BUFFLEN);   // memset(rcv_buff, 0, BUFFLEN);

   return 0;

}


/**
 * @brief  Set LS240 Module name, like "Magnet 5 Cooling Line”.
 * @note   
 * @retval 
 */
static int ls240_setModuleName()
{
   char moduleName[32];      // max 32 chars for module name
   char moduleNameCMD[64];
   int strl, rw_ret;

   printf("\nSet Module name => type in a name for this module(Max 32 chars): ");
   if (!fgets(moduleName, 32, stdin))
   {
      perror("fgets error:");
      exit(0);
   }

   dprintf("The length of the name is: %lu\n", strlen(moduleName));
   if ((strl = strlen(moduleName)) > 1)
   {
      moduleName[strl-1] = '\0';
      snprintf(moduleNameCMD, strl + 12, "%s%c%c%c%s%c%c", "MODNAME", ' ', ' ', '"', moduleName, '"', '\n');
   }
   else
   {
      printf("No name is given!\n");
      return -1;
   }
   tcflush(com_fd, TCIOFLUSH);

   dprintf("%lu bytes sent to 240. Set module name as: %s\n", strlen(moduleName), moduleName);
   rw_ret = write(com_fd, moduleNameCMD, strlen(moduleNameCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      return -1;
   }
   DBGPRINT("Command=%s has been sent to device 240\n", moduleNameCMD);
 
   //sleep(1);
   wait_10ms();
   ls240_queryModuleName();
   printf("Module name has been changed to: \"%s\".\n", moduleName);
   return 0;
}


/**
 * @brief  Reset device to factory default
 * @note   This command is disabled to make sure that user can not reset the device accidentally
 * @retval 
 */
static int ls240_setDFLT(void)
{
   char dfltCMD[16];
   int rw_ret;
   snprintf(dfltCMD, 6, "%s%c%s%c", "DFLT", ' ', "9XX9", '\n');
   tcflush(com_fd, TCIOFLUSH);
   dprintf("%lu bytes sent to 240. Set module name as: %s", strlen(dfltCMD), dfltCMD);
   rw_ret = write(com_fd, dfltCMD, strlen(dfltCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      return -1;
   }

   return 0;
}

/**
 * @brief  Query a channel's input name.
 * @note   
 * @param  input: The channel number to query(1-8).  If input == 0, program will prompt user to choose a channel to query. 
 * @retval 
 */
static int ls240_queryINNAME(int input)
{
   ssize_t rw_ret;
   char inName[16], instr[8];
   int inNum;
   tcflush(com_fd, TCIOFLUSH);
   
   if(input == 0)
   { 
      printf("Which channel's INNAME to query(1-8)? ");
      //if ((inNum = fgetc(stdin)) == EOF)
      //{
      //   perror("fgetc error:");
      //   exit(0);
      // }
      if (!fgets(instr, 8, stdin))
      {
         perror("fgets error:");
         exit(0);
      }
      inNum = atoi(instr);
   }
   else
   {
      inNum = input;
   }

   if(inNum < 1 || inNum > 8)
   {
      printf("Error, invalid input number! It must be 1 - 8\n");
      return -1;
   }

   //snprintf(inName, 11, "%s%c%c%c", "INNAME?", ' ', inNum, '\n');
   snprintf(inName, 11, "%s%c%d%c", "INNAME?", ' ', inNum, '\n');
   rw_ret = write(com_fd, inName, strlen(inName));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
   dprintf("Query device in name at channel %c, cmd=%s\n", inNum, inName);
   // get device reply
   if (ls240_getReply() < 0)
      printf("Error Reading device reply!\n");
   else
      printf("\nDevice's in-name at channel %c is: %s\n", inNum, rcv_buff);
   bzero(rcv_buff, BUFFLEN); // memset(rcv_buff, 0, BUFFLEN);

   return 0;
}

/**
 * @brief  Set a channel's input name. Max 15 characters. 
 *         This input name will be used to describe what the sensor is monitoring.
 * @note   
 * @retval 
 */
static int ls240_setINNAME(void)
{
   ssize_t rw_ret;
   char inName[15], instr[8];
   char setCMD[32];
   int inNum, inNameLen;
   tcflush(com_fd, TCIOFLUSH);

   printf("Which channel's INPUT NAME to set(1-8)? ");
   //if ((inNum = fgetc(stdin)) == EOF)
   //{
   //   perror("fgetc error:");
   //   exit(0);
   //}
   if (!fgets(instr, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   inNum = atoi(instr);
   if (inNum < 1 || inNum > 8)
   {
      printf("Error, invalid input number! It must be 1 - 8\n");
      return -1;
   }

   printf("Type in max. 15 characters to describe thie input/channel/sensor's purpose: ");
   if (!fgets(inName, 15, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   inNameLen = strlen(inName);
   inName[inNameLen - 1] = '\0';
   DBGPRINT("Inname is: %s\n", inName);
   //snprintf(inName, 11, "%s%c%c%c", "INNAME?", ' ', inNum, '\n');
   snprintf(setCMD, 32, "%s%c%d%c%s%c", "INNAME", ' ', inNum, ',', inName, '\n');
   DBGPRINT("Command send to device = %s\n", setCMD);
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      return -1;
   }

   wait_10ms();
   ls240_queryINNAME(inNum);

   printf("Channel %d input name has been set to: %s\n", inNum, inName);
   return 0;
}

/**
 * @brief  Query PROFIBUS address; 1 - 126
 * @note   
 * @retval 
 */
static int ls240_queryADDR(void)
{
   int rw_ret;
   //int tries;
   char queryAddrCMD[16];
   snprintf(queryAddrCMD, 7, "%s%c", "ADDR?", '\n');
   rw_ret = write(com_fd, queryAddrCMD, strlen(queryAddrCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      return -1;
   }
   DBGPRINT("write(): %d bytes data sent to device 240\n", (int)rw_ret);
   dprintf("Query device PROFIBUS address command=%s\n", queryAddrCMD);
   // get device reply
   if (ls240_getReply() < 0)
      printf("Error Reading device reply!\n");
   else
      printf("\nPROFIBUS address is: %s\n", rcv_buff);
   bzero(rcv_buff, BUFFLEN); // memset(rcv_buff, 0, BUFFLEN);

   return 0;
}


/**
 * @brief  Query front panel display brightness, 0=off, 1=25%, 2=%50, 3=%75, 4=100%
 * @note   
 * @retval 
 */
static int ls240_queryBRIGT(void)
{
   int rw_ret;
   char queryCMD[16];
   snprintf(queryCMD, 8, "%s%c", "BRIGT?", '\n');
   rw_ret = write(com_fd, queryCMD, strlen(queryCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      return -1;
   }
   DBGPRINT("write(): %d bytes data sent to device 240\n", (int)rw_ret);
   dprintf("Query device front display brightness command=%s\n", queryCMD);
   // get device reply
   if (ls240_getReply() < 0)
      printf("Error Reading device reply!\n");
   else
      printf("\nFront panel brightness is: %s : 0=off, 1=0.25, 2=0.5, 3=0.75, 4=1\n", rcv_buff);
   bzero(rcv_buff, BUFFLEN); // memset(rcv_buff, 0, BUFFLEN);

   return 0;
}


/**
 * @brief  Set Lakeshore 240 front panel display brightness
 * @note   
 * @retval 
 */
static int ls240_setBRIGT(void)
{
   int brigt;
   char setCMD[16], instr[8];
   int rw_ret;
   printf("\nType in a brightness number(0=off, 1=.25, 2=.5, 3=.75, 4=1): ");
   //if ((brigt = fgetc(stdin)) == EOF)
   //{
   //   perror("fgetc error:");
   //   exit(0);
   //}
   if (!fgets(instr, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   brigt = atoi(instr);
   if(brigt < 1 || brigt > 8)
   {
      printf("ERROR, invalid input number. It must be 1 - 8\n");
      return -1;
   }
   //snprintf(setCMD, 11, "%s%c%u%c", "BRIGT", ' ', (unsigned int)brigt, '\n');
   snprintf(setCMD, 11, "%s%c%d%c", "BRIGT", ' ', brigt, '\n');
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      return -1;
   }
   dprintf("Set device front display brightness, cmd=%s\n", setCMD);
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   wait_10ms();
   ls240_queryBRIGT();

   printf("Front panel brightness has been set.\n");

   return 0;
}

/**
 * @brief  Query curve header: <name>,<serial number>,<format>,<limit value>,<coefficient> 
 * @note   format:      1 = milli-Volts/Kelvin,  2 = V/K, 3 = OHM/K, 4 = log ohm/K
 *         input = 0, query all 8 input header,
 *         coefficient:  1 = negative, 2 = positive
 * 
 * @param  input: 1,2,3,4,5,6,7,8;  query the specific input header
 * @retval 
 */
static int ls240_queryCRVHDR(int input)
{
   char queryCMD[16];
   int rw_ret;
   int numloop;
   if (input < 0 || input > 8)
   {
      printf("Error, input must be one of 0,1,2,3,4,5,6,7,8\n");
      return -1;
   }

   if(input == 0)
      numloop = 9;
   else
      numloop = 2;

   for (int i = 1; i < numloop; i++)
   {
      if(input == 0)
         snprintf(queryCMD, 12, "%s%c%d%c", "CRVHDR?", ' ', i, '\n');
      else
         snprintf(queryCMD, 12, "%s%c%d%c", "CRVHDR?", ' ', input, '\n');

      dprintf("Query input %d curve header cmd = %s\n", i, queryCMD);
      rw_ret = write(com_fd, queryCMD, strlen(queryCMD));
      if (rw_ret < 0)
      {
         perror("Error write to 240!\n");
         close(com_fd);
         com_fd = -1;
         return -1;
      }
      dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

      // get device reply
      if (ls240_getReply() < 0)
         printf("Error Reading device reply!\n");
      else
         printf("Channel %d curve header is: %s\n", i, rcv_buff);
      memset(rcv_buff, 0, BUFFLEN);
   }
   printf("\n<name>,<serial number>,<format>,<limit value>,<coefficient>\n");
   printf("Format: 2 = V/K, 3 = ohm/K, 4 = log ohm/K. coefficient: 1 = negative, 2 = positive\n");
   return 0;
}

/**
 * @brief   set curve header
 * @note    This should work together with some other commands to upload a new curve to a channel
 * @retval 
 */
static int ls240_setCRVHDR(void)
{
   char setCMD[64], curvHeaderName[32];
   char instr[16];
   int rw_ret, strl;
   int input, format;
   double limitv;     // temperature limit value in kelvin
   unsigned short coefficient;

   // Which input curve header to set?
   printf("Which channel's curve header to set(1-8)? ");
   if (!fgets(instr, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }                                                           
   input = atoi(instr);
   DBGPRINT("input = %d\n", input);
   if (input < 1 || input > 8)
   {
      printf("Error, invalid input number! It must be 1 - 8\n");
      return -1;
   }

   // ask user for the curve header parameter: name, serial number, format, curve limit, coefficient
   
   printf("Type in curve header name(max 15 chars): ");
   if (!fgets(curvHeaderName, 16, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   dprintf("The length of the name is: %lu\n", strlen(curvHeaderName));
   if ((strl = strlen(curvHeaderName)) > 1)
   {
      curvHeaderName[strl - 1] = '\0';
      //snprintf(moduleNameCMD, strl + 12, "%s%c%c%c%s%c%c", "MODNAME", ' ', ' ', '"', moduleName, '"', '\n');
   }
   else
   {
      printf("No curv header name is given!\n");
      return -1;
   }

   printf("Give curve file Format(2 = V/K, 3 = ohm/K, 4 = log ohm/K)? ");
   if (!fgets(instr, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   format = atoi(instr);
   DBGPRINT("format = %d\n", format);
   if (format != 2 && format != 3 && format != 4)
   {
      printf("Error, invalid format number! It must be 2, 3 or 4 \n");
      return -1;
   }

   printf("Give curve limit in Kelvin(like 800.0000)? ");
   if (!fgets(instr, 16, stdin))
   {
      perror("fgets error:");
      return -1;
   }
   limitv = atoi(instr);
   DBGPRINT("limit = %f\n", limitv);

   printf("Give coefficient(1 = negative; 2 = positive)? ");
   if (!fgets(instr, 8, stdin))
   {
      perror("fgets error:");
      return -1;
   }
   coefficient = atoi(instr);
   DBGPRINT("coefficient = %d\n", coefficient);

   // prepare the cmd
   snprintf(setCMD, 64, "%s%c%d%c%c%s%c%c%c%s%c%c%d%c%f%c%d%c", "CRVHDR", ' ', input, ',', '"', curvHeaderName, '"', ',', '"', "NONE", '"', ',', format, ',', limitv, ',', coefficient, '\n');
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   wait_10ms();

   // set input type for the header

   return 0;
}

/**
 * @brief:  testing to upload a new curve to input(channel) 5 
 * @note :  the curve data is not real, just for testing that we can upload new customer curve to Lakeshore240
 * @retval  -1 = error, 0 = OK.
 * 
 *  Becareful! this will delete the channel 5's curve. 
 *  This function is disabled to make sure that user can not delete input 5 curve accidentally.
 */  
static int ls240_upload_curve(void)
{
   printf("Test upload curve\n");
   char crvCMD[64];
   char setCMD[128];
   int rw_ret;

   // query the first break point of the curve
   printf("This function will delete curve for input 5 and upload some un-real breakpoint data, so it is disabled!\n");
   //return 0;
   bzero(crvCMD, sizeof(crvCMD));
   snprintf(crvCMD, 64, "%s%c", "CRVPT? 7 1", '\n');
   rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   wait_10ms();

   // delete the old curve
   bzero(crvCMD, sizeof(crvCMD));
   snprintf(crvCMD, 64, "%s%c", "CRVDEL 7", '\n');      // delete curve 7
   rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   wait_10ms();

   // set new curve header
   snprintf(setCMD, 64, "%s%c%d%c%c%s%c%c%c%s%c%c%d%c%f%c%d%c", "CRVHDR", ' ', 7, ',', '"', "TEST_CURVE-1", '"', ',', '"', "NONE", '"',
            ',', 2, ',', 500.00, ',', 2, '\n');
   dprintf("Set curve header cmd = %s\n", setCMD);
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   wait_10ms();

   // set input type
   bzero(crvCMD, sizeof(crvCMD));
   snprintf(crvCMD, 64, "%s%c", "INTYPE 7,1,0,0,0,1,1", '\n');
   rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   wait_10ms();

   // Set some break point for testing
   for (int i = 1; i < 11; i++)
   {
      double sensorUnit = (double)i;
      double temp = (double)(i + 5);
      bzero(crvCMD, sizeof(crvCMD));
      snprintf(crvCMD, 64, "%s%c%d%c%f%c%f%c", "CRVPT 7", ',', i, ',', sensorUnit, ',', temp, '\n');
      DBGPRINT("%s\n", crvCMD);
      rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
      if (rw_ret < 0)
      {
         perror("Error write to 240!\n");
         close(com_fd);
         com_fd = -1;
         return -1;
      }
      dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
      wait_10ms();
   }

   wait_10ms();

   // query the new header
   bzero(crvCMD, sizeof(crvCMD));
   snprintf(crvCMD, 64, "%s%c", "CRVHDR? 7", '\n');
   rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);
   wait_10ms();

   // query the new input type
   bzero(crvCMD, sizeof(crvCMD));
   snprintf(crvCMD, 64, "%s%c", "INTYPE? 7", '\n');
   rw_ret = write(com_fd, crvCMD, strlen(crvCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   return 0;
}


/**
 * @brief  Query Lakeshore 240 curve break points
 * @note   
 * @retval 
 */
static int ls240_queryCRVPT(void)
{
   char queryCMD[16];
   int rw_ret;
   char instr[8];
   int inNum, endIndex;

   printf("Which channel's curve data table to query(1-8)? ");
   if (!fgets(instr, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   inNum = atoi(instr);
   DBGPRINT("inNum = %d\n", inNum);
   if (inNum < 1 || inNum > 8)
   {
      printf("Error, invalid input number! It must be 1 - 8\n");
      return -1;
   }

   printf("Give an end index (between 1-200): ");
   if (!fgets(instr, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   endIndex = atoi(instr);
   DBGPRINT("endIndex = %d\n", endIndex);
   if (endIndex < 1 || endIndex > 200)
   {
      printf("Error! Invalid end index! It must be in 1 - 199 range!\n");
      return -1;
   }

   for (int i = 1; i < endIndex; i++)
   {
      snprintf(queryCMD, 16, "%s%c%d%c%d%c", "CRVPT?", ' ', inNum, ',', i, '\n');
      dprintf("Query input %d curve data point %d cmd = %s\n", inNum, i, queryCMD);
      rw_ret = write(com_fd, queryCMD, strlen(queryCMD));
      if (rw_ret < 0)
      {
         perror("Error write to 240!\n");
         close(com_fd);
         com_fd = -1;
         return -1;
      }
      printf("write(): %d bytes data sent to 240\n", (int)rw_ret);

      // get device reply
      if (ls240_getReply() < 0)
         printf("Error Reading device reply!\n");
      else
         printf("Curve data table point %d: <Unit value>,<Temperature value> <<==>> %s\n", i, rcv_buff);
      memset(rcv_buff, 0, BUFFLEN);
   }
   return 0;
}

/**
 * @brief   Query PROFIBUS state (0 = Power on reset, 1 = waiting for parameterization, 2 = waiting for configuration
 *          3 = data exchange)
 * @note   
 * @retval  
 */
static int ls240_queryPROFISTAT(void)
{
   char queryCMD[32];
   int rw_ret;
   snprintf(queryCMD, 32, "%s%c%c", "PROFISTAT?", ' ', '\n');
   DBGPRINT("Command: %s sent to 240\n", queryCMD);
   rw_ret = write(com_fd, queryCMD, strlen(queryCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   // get device reply
   if (ls240_getReply() < 0)
      printf("Error Reading device reply!\n");
   else
      printf("\nPROFIBUS state: %s \n(0=power on reset, 1=waiting for parameterization, 2=waiting for configuration, 3=data exchange\n\n", rcv_buff);
   memset(rcv_buff, 0, BUFFLEN);   

   return 0;
}

// Query PROFIBUS slot
static int ls240_queryPROFISLOT(int input)
{
   char queryCMD[32];
   char *delim = ",\n\t";
   char *cp, *tokens[5];
   char instr[8];
   int inNum, rw_ret;

   if(input == 0)
   {   
      printf("Which input to query(1-8)? ");
      if (!fgets(instr, 8, stdin))
      {
         perror("fgets error:");
         exit(0);
      }
      inNum = atoi(instr);
      DBGPRINT("inNum = %d\n", inNum);
      if (inNum < 1 || inNum > 8)
      {
         printf("Error, invalid input number! It must be 1 - 8\n");
         return -1;
      }
   }
   else 
      inNum = input;

   snprintf(queryCMD, 32, "%s%c%d%c", "PROFISLOT?", ' ', inNum, '\n');
   DBGPRINT("Command: %s sent to device 240\n", queryCMD);
   rw_ret = write(com_fd, queryCMD, strlen(queryCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   // get device reply
   if (ls240_getReply() < 0)
   {
      printf("Error Reading device reply!\n");
      return -1;
   }
   cp = rcv_buff;
   pickOutToken(tokens, cp, delim);
   printf("\nPROFISLOT Configuration: Input %d <==> Channel/Slot %s, Sensor Unit: %s\n\n", inNum, tokens[0], tokens[1]);
   memset(rcv_buff, 0, BUFFLEN);   

   return 0;
}

// Query PROFIBUS number
static int ls240_queryPROFINUM(void)
{
   char queryCMD[32];
   int rw_ret;
   snprintf(queryCMD, 32, "%s%c%c", "PROFINUM?", ' ', '\n');
   DBGPRINT("Command: %s send to device\n", queryCMD);
   rw_ret = write(com_fd, queryCMD, strlen(queryCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("write(): %d bytes data sent to 240\n", (int)rw_ret);

   // get device reply
   if (ls240_getReply() < 0)
   {
      printf("Error Reading device reply!\n");
      return -1;
   }

   printf("\nThe PROFIBUS number is: %s\n\n", rcv_buff);
   memset(rcv_buff, 0, BUFFLEN);      

   return 0;
}

// Set PROFIBUS number
static int ls240_setPROFINUM()
{
   char setCMD[32];
   int rw_ret;
   char instr[8];
   int inNum;

   printf("Give a PROFIBUS number(1-8): ");
   if (!fgets(instr, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   inNum = atoi(instr);
   DBGPRINT("inNum = %d\n", inNum);
   if (inNum < 1 || inNum > 8)
   {
      printf("Error, invalid Profibus number! It must be 1 - 8\n");
      return -1;
   }   

   snprintf(setCMD, 32, "%s%c%d%c", "PROFINUM", ' ', inNum, '\n');
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("\nPROFIBUS number has been set as: %d\n\n", inNum);

   wait_10ms();
   ls240_queryPROFINUM();

   return 0;
}

// Set PROFIBUS slot  
static int ls240_setPROFISLOT(void)
{
   char setCMD[32];
   char input[8];
   int rw_ret;
   int slotNum, inputNum, tempUnit;
   // ask user which slot to configure
   printf("Which PROFIBUS slot to configure(1-8)? ");
   if (!fgets(input, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   slotNum = atoi(input);
   DBGPRINT("Slot Num = %d\n", slotNum);
   if (slotNum < 1 || slotNum > 8)
   {
      printf("Error, invalid slot number! It must be 1 - 8\n");
      return -1;
   }

   // let user decide which input should be associated to this slot
   printf("Which input should be associated with this slot(1-8)? ");
   bzero(input, sizeof(input));
   if (!fgets(input, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   inputNum = atoi(input);
   DBGPRINT("Input Num = %d\n", inputNum);
   if (inputNum < 1 || inputNum > 8)
   {
      printf("Error, invalid input number! It must be 1 - 8\n");
      return -1;
   }

   // Tell which temperature unit the slot is using
   printf("What temperature unit the slot has (1=Kelvin, 2=Celsius, 3=sensor, 4=Fahrenheit)? ");
   if (!fgets(input, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   tempUnit = atoi(input);
   DBGPRINT("Input Num = %d\n", tempUnit);
   if (tempUnit != 1 && tempUnit != 2 && tempUnit != 3 && tempUnit != 4)
   {
      printf("Error, invalid input number! It must be 1 - 4\n");
      return -1;
   }

   snprintf(setCMD, 32, "%s%c%d%c%d%c%d%c", "PROFISLOT", ' ', slotNum, ',', inputNum, ',', tempUnit, '\n');
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("\nPROFIBUS slot has been set as: Input %d <===> Slot %d; Temperature unit: %d\n\n", inputNum, slotNum, tempUnit);

   wait_10ms();
   ls240_queryPROFISLOT(inputNum);
   
   return 0;
}

/**
 *  Set PROFIBUS address:  1 - 126. Address 126 indicate that the address is not configured and 
 *  can be set by a PROFIBUS master.
 *  When set the PROFIBUS address to 126, the PROFIBUS enter into state   0 --- "POWER ON/RESET", 
 *  When set the PROFIBUS address to 1-125, the PROFIBUS enter into state 1 --- "Waiting for parameterization"
 *  2 = Waiting for configuration,   3 = Data exchange
 */
static int ls240_setAddr(void)
{
   char setCMD[32];
   int rw_ret;
   char input[8];
   int addr;

   printf("Which PROFIBUS address should this device use (1 - 125)? ");
   if (!fgets(input, 8, stdin))
   {
      perror("fgets error:");
      exit(0);
   }
   addr = atoi(input);
   snprintf(setCMD, 32, "%s%c%d%c", "ADDR", ' ', addr, '\n');

   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }
   dprintf("\nPROFIBUS address has been set to: %d\n\n", addr);

   wait_10ms();   
   ls240_queryADDR();

   return 0;
}

/**
 *  STOP PROFIBUS
 *
 */
static int ls240_setPROFISTOP(void)
{
   char setCMD[32];
   int rw_ret;

   snprintf(setCMD, 32, "%s%c%d%c", "PROFISTOP", ' ', 0, '\n');
   DBGPRINT("Command sent to device: %s\n", setCMD);
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }

   snprintf(setCMD, 32, "%c", '\n');
   rw_ret = write(com_fd, setCMD, strlen(setCMD));
   if (rw_ret < 0)
   {
      perror("Error write to 240!\n");
      close(com_fd);
      com_fd = -1;
      return -1;
   }     

   wait_10ms();
   ls240_queryPROFISTAT();

   return 0;
}

/**
 * @brief  run some basic quieries if user does not give specific Lakeshore240 command as tool's command line argument.
 * @note   
 * @retval None
 */
static void ls240_run_all_cmd(void)
{
   ls240_queryDeviceID();
   ls240_queryModuleName();
   ls240_queryIntype();
   ls240_readTemperatures();

}


/**
 * @brief  Check to see what command we got from the commandline argument
 * @note   
 * @param  *str: user issued command 
 * @retval 
 */
static int ls240_checkCMD(char *str)
{
   int cmd;
   if(strcmp(str, "Q_IDN") == 0)
      cmd = Q_IDN;
   else if (strcmp(str, "Q_INNAME") == 0)
      cmd = Q_INNAME;
   else if (strcmp(str, "Q_KRDG") == 0)
      cmd = Q_KRDG;
   else if (strcmp(str, "Q_MODNAME") == 0)
      cmd = Q_MODNAME;
   else if(strcmp(str, "Q_INTYPE") == 0)
      cmd = Q_INTYPE;
   else if(strcmp(str, "Q_BRIGT") == 0)
      cmd = Q_BRIGT;
   else if(strcmp(str, "Q_CRDG") == 0)
      cmd = Q_CRDG;
   else if (strcmp(str, "Q_CRVHDR") == 0)
      cmd = Q_CRVHDR;
   else if (strcmp(str, "CRVHDR") == 0)
      cmd = CRVHDR;
   else if (strcmp(str, "Q_CRVPT") == 0)
      cmd = Q_CRVPT;
   else if (strcmp(str, "Q_FILTER") == 0)
      cmd = Q_FILTER;
   else if (strcmp(str, "Q_FRDG") == 0)
      cmd = Q_FRDG;
   else if (strcmp(str, "Q_PROFINUM") == 0)
      cmd = Q_PROFINUM;
   else if (strcmp(str, "Q_PROFISLOT") == 0)
      cmd = Q_PROFISLOT;
   else if (strcmp(str, "Q_PROFISTAT") == 0)
      cmd = Q_PROFISTAT;
   else if (strcmp(str, "Q_RDGST") == 0)
      cmd = Q_RDGST;
   else if (strcmp(str, "Q_SRDG") == 0)
      cmd = Q_SRDG;
   else if (strcmp(str, "Q_ADDR") == 0)
      cmd = Q_ADDR;
   else if (strcmp(str, "ADDR") == 0)
      cmd = ADDR;
   else if (strcmp(str, "BRIGT") == 0)
      cmd = BRIGT;
   else if (strcmp(str, "CRVDEL") == 0)
      cmd = CRVDEL;
   else if (strcmp(str, "CRVPT") == 0)
      cmd = CRVPT;
   else if (strcmp(str, "DFLT") == 0)
      cmd = DFLT;
   else if (strcmp(str, "FILTER") == 0)
      cmd = FILTER;
   else if (strcmp(str, "INNAME") == 0)
      cmd = INNAME;
   else if (strcmp(str, "INTYPE") == 0)
      cmd = INTYPE;
   else if (strcmp(str, "MODNAME") == 0)
      cmd = MODNAME;
   else if (strcmp(str, "PROFINUM") == 0)
      cmd = PROFINUM;
   else if (strcmp(str, "PROFISLOT") == 0)
      cmd = PROFISLOT;
   else if (strcmp(str, "UPLOADCRV") == 0)
      cmd = UPLOADCRV;
   else if (strcmp(str, "PROFISTOP") == 0)
      cmd = PROFISTOP;
   else
   {
      printf("\nUnknow command: %s!\n Type \"lakeshore240_usb_com -h\" for help!\n\n", str);
      cmd = -1;
   }

   DBGPRINT("This is number %d COMMAND. \n", cmd);

   return cmd;
}

static void ls240_usage(void)
{
   printf("There are two ways to use this tool:\n  1.) execute this tool without commandline argument: \
   all implemented commands will be excuted\n  2.) execute this tool with a Lakeshore240 command as command line \
   parameter : only one Lakeshore240 command will be executed\n");
   printf("Usage: lakeshore_usb_com  [COMMAND_NAME]\n");
   printf("\nLakeshore240 command set:\n");
   printf("             Q_IDN --- query device ID\n \
            Q_ADDR --- query PROFIBUS address\n \
            ADDR --- set PROFIBUS address\n \
            Q_BRIGT --- query display brightness\n \
            BRIGT --- set display brightness\n \
            Q_CRDG --- query temperature in celsius\n \
            CRVDEL --- delete curve\n \
            Q_CRVHDR --- query curve header\n \
            CRVHDR --- set curve header \n \
            Q_CRVPT --- query curve point\n \
            CRVPT --- set curve point\n \
            DFLT --- restore factory default\n \
            Q_FILTER --- query input filter parameter\n \
            FILTER  --- set input filter parameter\n \
            Q_FRDG --- query fahrenheit reading\n \
            Q_INNAME --- query in name\n \
            INNAME --- set in name\n \
            Q_INTYPE --- query in type\n \
            INTYPE --- set in type\n \
            Q_KRDG --- query temperature in kelvin\n \
            Q_MODNAME --- query device module name\n \
            MODNAME --- set device module name\n \
            Q_PROFINUM --- query PROFIBUS slot count\n \
            PROFINUM --- set PROFIBUS slot count\n \
            Q_PROFISLOT --- query PROFIBUS configuration\n \
            PROFISLOT --- set PROFIBUS configuration\n \
            Q_PROFISTAT --- query PROFIBUS connection status\n \
            Q_RDGST --- query input reading status\n \
            Q_SRDG --- query sensor units input reading\n \
            PROFISTOP --- stop PROFIBUS communication\n \
            UPLOADCRV --- only for testing upload curve process \n\n");
}

/**
 * @brief  Main function
 * @note   
 * @param  argc: 
 * @param  *argv[1]: command to run, or "-h" for help 
 * @retval 
 */
int main(int argc, char *argv[]) 
{

   //struct timeval tv;
   //suseconds_t u_seconds = 0;  /* default micro-seconds.  */
   //char *tty_names[21];
   //dprintf("argc=%d, argv[1]=%s\n", argc, argv[1]);
   // Initialize virtual port
   if (init_serial_ports() < 0)
   {
      printf("Serial port initialization error!\n");
      exit(-1);
   }
   
   if(argc > 1)
   {
      if(strncmp(argv[1], "-h", 2) == 0)
      {
         ls240_usage();
         return 0;
      }

      if (strncmp(argv[1], "-h", 2) != 0 && argc == 2)
      {
         int cmd = ls240_checkCMD(argv[1]);
         if (cmd > 0)
         {
            switch (cmd)
            {
               case Q_IDN:
                  ls240_queryDeviceID();
                  break;
               case Q_INTYPE:
                  ls240_queryIntype();
                  break;
               case Q_KRDG:
                  ls240_readTemperatures();
                  break;
               case Q_MODNAME:
                  ls240_queryModuleName();
                  break;
                  case MODNAME:
                  ls240_setModuleName();
                  break;
               case DFLT:
                  ls240_setDFLT();
                  break;
               case Q_INNAME:
                  ls240_queryINNAME(0);
                  break;
               case Q_ADDR:
                  ls240_queryADDR();
                  break;
               case Q_BRIGT:
                  ls240_queryBRIGT();
                  break;
               case BRIGT:
                  ls240_setBRIGT();
                  break;
               case Q_CRVHDR:
                  ls240_queryCRVHDR(0);
                  break;
               case CRVHDR:
                  ls240_setCRVHDR();
                  break;
               case Q_CRVPT:
                  ls240_queryCRVPT();
                  break;
               case UPLOADCRV:           
                  ls240_upload_curve();
                  break;
               case INNAME:
                  ls240_setINNAME();
                  break;
               case Q_PROFISTAT:
                  ls240_queryPROFISTAT();
                  break;
               case Q_PROFISLOT:
                  ls240_queryPROFISLOT(0);
                  break;
               case PROFISLOT:
                  ls240_setPROFISLOT();
                  break;
               case Q_PROFINUM:
                  ls240_queryPROFINUM();
                  break;
               case PROFINUM:
                  ls240_setPROFINUM();
                  break;
               case ADDR:
                  ls240_setAddr();
                  break;
               case PROFISTOP:
                  ls240_setPROFISTOP();
                  break;
               default:
                  printf("\nCommand has not been implemented yet!\n\n");
                  break;
            }
         }
      }
   }
   else if(argc == 1)
      ls240_run_all_cmd();

   close(com_fd);

   return 0;

}
